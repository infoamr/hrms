<?php
namespace App\Models;

use CodeIgniter\Model;
	
class DailyReportModel extends Model {
 
    protected $table = 'ci_daily_report';

    protected $primaryKey = 'sid';
    
	// get all fields of user roles table
    protected $allowedFields = ['sid','employee_name','system_name','active_hours','passive_hours','total_hours', 'uploaded_date'];
	
	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;
	
}
?>