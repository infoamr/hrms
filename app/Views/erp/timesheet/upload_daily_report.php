<?php
use App\Models\RolesModel;
use App\Models\UsersModel;
use App\Models\SystemModel;
//$encrypter = \Config\Services::encrypter();
$SystemModel = new SystemModel();
$RolesModel = new RolesModel();
$UsersModel = new UsersModel();

$session = \Config\Services::session();
$usession = $session->get('sup_username');
$request = \Config\Services::request();

$user_info = $UsersModel->where('user_id', $usession['sup_user_id'])->first();
?>

<div class="row m-b-1">
  <div class="col-md-12">
    <div class="card user-profile-list mb-4">
      <div class="card-header">
        <h5>
          <?= lang('Dashboard.xin_upload_daily_report');?>
        </h5>
        <?php if(in_array('upattendance2',staff_role_resource()) || $user_info['user_type'] == 'company') {?>
        <div class="card-header-right">
          <?php $attr = array('name' => 'upload_daily_report', 'id' => 'xin-form', 'autocomplete' => 'off');?>
          <?php $hid = array('user_id' => 1);?>
          <?php echo form_open_multipart('erp/timesheet/importFile', $attr, $hid);?>
          <input type="file" class="" name="file" value=""></input>
          <button type="submit" class="btn btn-success">
          <?= lang('Main.xin_save');?>
          </button>
          <?= form_close(); ?> 
        </div>
        <?php } ?>
      </div>
      
    </div>
  </div>
</div>
