<?php
use CodeIgniter\I18n\Time;

use App\Models\RolesModel;
use App\Models\UsersModel;
use App\Models\SystemModel;
use App\Models\ConstantsModel;
use App\Models\LeaveModel;
use App\Models\TicketsModel;
use App\Models\ProjectsModel;
use App\Models\MembershipModel;
use App\Models\TransactionsModel;
use App\Models\CompanymembershipModel;
use App\Models\DailyReportModel;

//$encrypter = \Config\Services::encrypter();
$SystemModel = new SystemModel();
$RolesModel = new RolesModel();
$UsersModel = new UsersModel();
$LeaveModel = new LeaveModel();
$TicketsModel = new TicketsModel();
$ProjectsModel = new ProjectsModel();
$MembershipModel = new MembershipModel();
$TransactionsModel = new TransactionsModel();
$ConstantsModel = new ConstantsModel();
$CompanymembershipModel = new CompanymembershipModel();
$DailyReportModel = new DailyReportModel();
$activ_info = $DailyReportModel->orderBy('sid', 'DESC')->findAll();


$session = \Config\Services::session();
$usession = $session->get('sup_username');
$request = \Config\Services::request();
$xin_system = erp_company_settings();
$user_info = $UsersModel->where('user_id', $usession['sup_user_id'])->first();
$company_id = user_company_info();
$total_staff = $UsersModel->where('company_id', $company_id)->where('user_type','staff')->countAllResults();
$total_projects = $ProjectsModel->where('company_id',$company_id)->countAllResults();
$total_tickets = $TicketsModel->where('company_id',$company_id)->countAllResults();
$open = $TicketsModel->where('company_id',$company_id)->where('ticket_status', 1)->countAllResults();
$closed = $TicketsModel->where('company_id',$company_id)->where('ticket_status', 2)->countAllResults();
  
// membership
$company_membership = $CompanymembershipModel->where('company_id', $usession['sup_user_id'])->first();
$subs_plan = $MembershipModel->where('membership_id', $company_membership['membership_id'])->first();
$current_time = Time::now('Asia/Karachi');
$company_membership_details = company_membership_details();
if($company_membership_details['diff_days'] < 8){
  $alert_bg = 'alert-danger';
} else {
  $alert_bg = 'alert-warning';
} 
?>

<?php if(in_array('attendance',staff_role_resource()) || in_array('upattendance1',staff_role_resource()) || in_array('monthly_time',staff_role_resource()) || in_array('overtime_req1',staff_role_resource())|| $user_info['user_type'] == 'company') {?>

<div class="row m-b-1">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header with-elements">
        <h5>
          <?= lang('Main.xin_filter');?>
          <?= 'Report';?>
        </h5>
      </div>
      <?php $attributes = array('name' => 'update_attendance_report', 'id' => 'xin-form', 'autocomplete' => 'off');?>
      <?php $hidden = array('user_id' => 1);?>
      <?php echo form_open('erp/timesheet/update_attendance_list', $attributes, $hidden);?>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="date">
                    <?= lang('Dashboard.xin_start_date');?>
                  </label>
                  <div class="input-group">
                    <input class="form-control date" placeholder="<?= lang('Main.xin_e_details_date');?>" id="attendance_date" name="attendance_date" type="text" value="<?php echo date('Y-m-d');?>">
                    <div class="input-group-append"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="date">
                    <?= lang('Dashboard.xin_end_date');?>
                  </label>
                  <div class="input-group">
                    <input class="form-control date" placeholder="<?= lang('Main.xin_e_details_date');?>" id="attendance_date" name="attendance_date" type="text" value="<?php echo date('Y-m-d');?>">
                    <div class="input-group-append"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div>
                  </div>
                </div>
              </div>
              <?php if($user_info['user_type'] == 'company'){?>
              <?php $staff_info = $UsersModel->where('company_id', $usession['sup_user_id'])->where('user_type','staff')->where('is_active',1)->findAll();?>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="first_name">
                    <?= lang('Dashboard.dashboard_employee');?>
                  </label>
                  <select class="form-control" name="employee_id" id="employee_id" data-plugin="select_hrm" data-placeholder="<?= lang('Dashboard.dashboard_employee');?>">
                    <?php foreach($staff_info as $staff) {?>
                    <option value="<?= $staff['user_id']?>">
                    <?= $staff['first_name'].' '.$staff['last_name'] ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <?php } ?>
              <div class="col-md-3">
                <label for="date">.</label>
                <div class="input-group">
                  <button type="submit" class="btn btn-primary">
                  <?= lang('Main.xin_filter');?>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xl-6 col-md-12">
    
    <div class="row">
      <div class="col-xl-12 col-md-12">
        <div class="card">
          <div class="card-body">
            <h6>
              <?= 'Staff wise Report';?>
            </h6>
            <div class="row d-flex justify-content-center align-items-center">
              <div class="col">
                <div id="activ-wise-chart"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-6 col-md-12">
    <div class="card">
      <div class="card-header">
        <h5>
          <?= 'ActivTrak Daily Report';?>
        </h5>
      </div>
      <div class="card-body">
        <div class="row pb-2">
          <div class="col-auto m-b-10">
            <h3 class="mb-1">
              <?= number_to_currency(total_payroll(), $xin_system['default_currency'],null,2);?>
            </h3>
            <span>
            <?= lang('Main.xin_total');?>
            </span> </div>
          <div class="col-auto m-b-10">
            <h3 class="mb-1">
              <?= number_to_currency(payroll_this_month(), $xin_system['default_currency'],null,2);?>
            </h3>
            <span>
            <?= lang('Payroll.xin_payroll_this_month');?>
            </span> </div>
        </div>
        <div id="erp-payroll-chart"></div>
      </div>
    </div>
    
  </div>
</div>

<hr class="border-light m-0 mb-3">
<?php } ?>

<div class="row m-b-1">
  <div class="col-md-12">
    <div class="card user-profile-list mb-4">
      <div class="card-header">
        <h5>
          <?= 'ActivTrak Report';?>
        </h5>
      </div>
      <div class="card-body">
        <div class="box-datatable table-responsive">
          <table class="datatables-demo table table-striped table-bordered" id="xin_table">
            <thead>
              <tr>
                <th><?= lang('Dashboard.dashboard_employee');?></th>
                <th><?= lang('Dashboard.xin_system_name');?></th>
                <th><?= lang('Dashboard.xin_active_hours');?></th>
                <th><?= lang('Dashboard.xin_passive_hours');?></th>
                <th><?= lang('Dashboard.xin_total_hours');?></th>
                <th><?= lang('Main.xin_e_details_date');?></th>
              </tr>
            </thead>

            <?php foreach($activ_info as $acin) {?>
                <tr>
                <th><?= $acin['employee_name'];?></th>
                <th><?= $acin['system_name'];?></th>
                <th><?= $acin['active_hours'];?></th>
                <th><?= $acin['passive_hours'];?></th>
                <th><?= $acin['total_hours'];?></th>
                <th><?= $acin['uploaded_date'];?></th>
              </tr>
            <?php } ?>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
