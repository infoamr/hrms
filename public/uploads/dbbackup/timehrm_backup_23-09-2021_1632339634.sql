# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------


#
# Delete any existing table `ci_advance_salary`
#

DROP TABLE IF EXISTS `ci_advance_salary`;


#
# Table structure of table `ci_advance_salary`
#

CREATE TABLE `ci_advance_salary` (
  `advance_salary_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `salary_type` varchar(100) DEFAULT NULL,
  `month_year` varchar(255) NOT NULL,
  `advance_amount` decimal(65,2) NOT NULL,
  `one_time_deduct` varchar(50) NOT NULL,
  `monthly_installment` decimal(65,2) NOT NULL,
  `total_paid` decimal(65,2) NOT NULL,
  `reason` text NOT NULL,
  `status` int(11) DEFAULT NULL,
  `is_deducted_from_salary` int(11) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`advance_salary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_advance_salary (1 records)
#
 
INSERT INTO `ci_advance_salary` VALUES ('1', '2', '24', 'loan', '2021-09', '30000.00', '0', '15000.00', '0.00', 'testing', '0', '0', '08-09-2021 09:40:12') ;
#
# End of data contents of table ci_advance_salary
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------


#
# Delete any existing table `ci_announcements`
#

DROP TABLE IF EXISTS `ci_announcements`;


#
# Table structure of table `ci_announcements`
#

CREATE TABLE `ci_announcements` (
  `announcement_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `department_id` varchar(255) NOT NULL,
  `title` varchar(200) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `published_by` int(111) NOT NULL,
  `summary` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`announcement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_announcements (0 records)
#

#
# End of data contents of table ci_announcements
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------


#
# Delete any existing table `ci_assets`
#

DROP TABLE IF EXISTS `ci_assets`;


#
# Table structure of table `ci_assets`
#

CREATE TABLE `ci_assets` (
  `assets_id` int(111) NOT NULL AUTO_INCREMENT,
  `assets_category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `company_asset_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `purchase_date` varchar(255) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL,
  `warranty_end_date` varchar(255) NOT NULL,
  `asset_note` text NOT NULL,
  `asset_image` varchar(255) NOT NULL,
  `is_working` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`assets_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_assets (21 records)
#
 
INSERT INTO `ci_assets` VALUES ('1', '169', '174', '2', '12', 'AMR-DESK-001', 'Ram-11', '2019-04-22', '', 'Dell', 'Ram-11', '', 'This Computer assigned to Chaitnya Rangappa.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 12:05:30') ; 
INSERT INTO `ci_assets` VALUES ('2', '169', '174', '2', '29', 'AMR-DESK-002', 'Asha Gowda', '2021-08-16', '', 'Dell', '', '', 'This Computer assigned to Asha Gowda.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 12:11:43') ; 
INSERT INTO `ci_assets` VALUES ('3', '169', '174', '2', '16', 'AMR-DESK-003', 'RAM-3', '2019-04-03', '', 'Dell', '', '', 'This Computer Assigned to Chaithra Eranna.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 12:18:42') ; 
INSERT INTO `ci_assets` VALUES ('4', '169', '174', '2', '30', 'AMR-DESK-004', 'Chaitra Hemanth', '2019-06-18', '', 'Dell', '', '', 'This Computer Assigned to Chaitra Hemanth.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 12:21:18') ; 
INSERT INTO `ci_assets` VALUES ('5', '169', '174', '2', '4', 'AMR-DESK-005', 'DESKTOP-SMH0AC9', '2019-03-18', '', 'Dell', '', '', 'This Computer Assigned to Deinesh Kumar.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:33:21') ; 
INSERT INTO `ci_assets` VALUES ('6', '169', '174', '2', '17', 'AMR-DESK-006', 'RAM-10', '2018-01-10', '', 'Dell', '', '', 'This Computer Assigned to Hitesh Kumar.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:35:16') ; 
INSERT INTO `ci_assets` VALUES ('7', '169', '174', '2', '14', 'AMR-DESK-007', 'RAM-6', '2019-08-13', '', 'Dell', '', '', 'This Computer Assigned to Latha Yallappa.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:37:06') ; 
INSERT INTO `ci_assets` VALUES ('8', '169', '174', '2', '15', 'AMR-DESK-008', 'RAM-5', '', '', 'Dell', '', '', 'This Computer Assigned to Mahalakshmi BG.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:39:14') ; 
INSERT INTO `ci_assets` VALUES ('9', '169', '174', '2', '26', 'AMR-DESK-009', 'Ram-9', '2019-07-19', '', 'Dell', '', '', 'This Computer Assigned to Nethravathi Manjunath.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:42:14') ; 
INSERT INTO `ci_assets` VALUES ('10', '169', '174', '2', '7', 'AMR-DESK-010', 'RAM-8', '2019-08-06', '', 'Dell', '', '', 'This Computer Assigned to Pushpa Latha.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:44:31') ; 
INSERT INTO `ci_assets` VALUES ('11', '169', '174', '2', '11', 'AMR-DESK-011', 'DESKTOP-DBUJFC0', '2019-06-18', '', 'Dell', '', '', 'This Computer Assigned to Rashmi Narasimhaiah.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:47:20') ; 
INSERT INTO `ci_assets` VALUES ('12', '169', '174', '2', '13', 'AMR-DESK-012', 'DESKTOP MP2HGIC', '2019-06-06', '', 'Dell', '', '', 'This Computer Assigned to Sabeeha Khanum.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:54:18') ; 
INSERT INTO `ci_assets` VALUES ('13', '169', '174', '2', '6', 'AMR-DESK-013', 'DESKTOP-6SDEVLU', '2019-07-24', '', 'Dell', '', '', 'This Computer Assigned to Santosh Kumar TS.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 01:57:40') ; 
INSERT INTO `ci_assets` VALUES ('14', '169', '174', '2', '27', 'AMR-DESK-014', 'Sharada M', '', '', 'Dell', '', '', 'This Computer Assigned to Sharda Manjunath.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:00:37') ; 
INSERT INTO `ci_assets` VALUES ('15', '169', '174', '2', '10', 'AMR-DESK-015', 'DESKTOP-GPCL9CN', '2019-06-11', '', 'Dell', '', '', 'This Computer Assigned to Srilatha Hovindaraju.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:02:52') ; 
INSERT INTO `ci_assets` VALUES ('16', '169', '174', '2', '5', 'AMR-DESK-016', 'RAM-2', '2019-05-29', '', 'Dell', '', '', 'This Computer Assigned to Suresh.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:07:09') ; 
INSERT INTO `ci_assets` VALUES ('17', '169', '174', '2', '28', 'AMR-DESK-017', 'Vaishnavi Gowda', '2021-08-24', '', 'Dell', '', '', 'This Computer Assigned to Vaishnavi Gowda.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:09:27') ; 
INSERT INTO `ci_assets` VALUES ('18', '169', '174', '2', '18', 'AMR-DESK-018', 'DESKTOP-K2B42NR', '2019-02-28', '', 'Dell', '', '', 'This Computer Assigned to Vandana KJ.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:11:25') ; 
INSERT INTO `ci_assets` VALUES ('19', '169', '174', '2', '8', 'AMR-DESK-019', 'RAM-7', '2019-07-04', '', 'Dell', '', '', 'This Computer Assigned to Venkat Swamy S R.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:13:41') ; 
INSERT INTO `ci_assets` VALUES ('20', '169', '174', '2', '3', 'AMR-DESK-020', 'DESKTOP-4SJ2JAJ', '2019-05-02', '', 'Dell', '', '', 'This Computer Assigned to Vikram Ranganathappa.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:16:35') ; 
INSERT INTO `ci_assets` VALUES ('21', '169', '174', '2', '9', 'AMR-DESK-021', 'DESKTOP-3SNK1D3', '2019-05-13', '', 'Dell', '', '', 'This Computer Assigned to Vinay Chandrashekariah.', 'dell-desktop-computer-500x500.jpg', '1', '22-09-2021 02:18:24') ;
#
# End of data contents of table ci_assets
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------


#
# Delete any existing table `ci_awards`
#

DROP TABLE IF EXISTS `ci_awards`;


#
# Table structure of table `ci_awards`
#

CREATE TABLE `ci_awards` (
  `award_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(200) NOT NULL,
  `award_type_id` int(200) NOT NULL,
  `associated_goals` text,
  `gift_item` varchar(200) NOT NULL,
  `cash_price` decimal(65,2) NOT NULL,
  `award_photo` varchar(255) NOT NULL,
  `award_month_year` varchar(200) NOT NULL,
  `award_information` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`award_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_awards (0 records)
#

#
# End of data contents of table ci_awards
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------


#
# Delete any existing table `ci_company_membership`
#

DROP TABLE IF EXISTS `ci_company_membership`;


#
# Table structure of table `ci_company_membership`
#

CREATE TABLE `ci_company_membership` (
  `company_membership_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `membership_id` int(11) NOT NULL,
  `subscription_type` varchar(25) NOT NULL,
  `update_at` varchar(100) DEFAULT NULL,
  `created_at` varchar(100) NOT NULL,
  PRIMARY KEY (`company_membership_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_company_membership (0 records)
#

#
# End of data contents of table ci_company_membership
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------


#
# Delete any existing table `ci_complaints`
#

DROP TABLE IF EXISTS `ci_complaints`;


#
# Table structure of table `ci_complaints`
#

CREATE TABLE `ci_complaints` (
  `complaint_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `complaint_from` int(111) NOT NULL,
  `title` varchar(255) NOT NULL,
  `complaint_date` varchar(255) NOT NULL,
  `complaint_against` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`complaint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_complaints (0 records)
#

#
# End of data contents of table ci_complaints
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------


#
# Delete any existing table `ci_contract_options`
#

DROP TABLE IF EXISTS `ci_contract_options`;


#
# Table structure of table `ci_contract_options`
#

CREATE TABLE `ci_contract_options` (
  `contract_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `salay_type` varchar(200) DEFAULT NULL,
  `contract_tax_option` int(11) NOT NULL,
  `is_fixed` int(11) NOT NULL,
  `option_title` varchar(200) DEFAULT NULL,
  `contract_amount` decimal(65,2) DEFAULT '0.00',
  PRIMARY KEY (`contract_option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_contract_options (38 records)
#
 
INSERT INTO `ci_contract_options` VALUES ('9', '12', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('40', '9', 'allowances', '1', '1', 'HRA', '5000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('4', '3', 'allowances', '1', '1', 'HRA', '20000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('5', '3', 'allowances', '1', '1', 'Special Allowance', '8000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('6', '3', 'allowances', '1', '1', 'Night Shift Allowance', '2000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('7', '12', 'statutory', '0', '1', 'EPF', '1800.00') ; 
INSERT INTO `ci_contract_options` VALUES ('8', '12', 'statutory', '0', '1', 'ESI', '113.00') ; 
INSERT INTO `ci_contract_options` VALUES ('10', '12', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('11', '16', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('12', '16', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('13', '4', 'allowances', '1', '1', 'HRA', '15000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('14', '4', 'allowances', '1', '1', 'Conveyance', '10000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('15', '17', 'allowances', '1', '1', 'HRA', '5000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('16', '17', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('17', '14', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('18', '14', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('19', '15', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('20', '15', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('21', '7', 'allowances', '1', '1', 'HRA', '2500.00') ; 
INSERT INTO `ci_contract_options` VALUES ('22', '7', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('23', '11', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('24', '11', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('25', '13', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('26', '13', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('27', '6', 'allowances', '1', '1', 'HRA', '15000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('28', '6', 'allowances', '1', '1', 'Conveyance', '11000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('29', '19', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('30', '19', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('31', '10', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('32', '10', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('33', '5', 'allowances', '1', '1', 'HRA', '20000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('34', '5', 'allowances', '1', '1', 'Conveyance', '7000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('35', '18', 'allowances', '1', '1', 'HRA', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('36', '18', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('37', '8', 'allowances', '1', '1', 'HRA', '5000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('38', '8', 'allowances', '1', '1', 'Conveyance', '0.00') ; 
INSERT INTO `ci_contract_options` VALUES ('39', '3', 'allowances', '1', '1', 'Conveyance', '17000.00') ; 
INSERT INTO `ci_contract_options` VALUES ('41', '9', 'allowances', '1', '1', 'Conveyance', '0.00') ;
#
# End of data contents of table ci_contract_options
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------


#
# Delete any existing table `ci_countries`
#

DROP TABLE IF EXISTS `ci_countries`;


#
# Table structure of table `ci_countries`
#

CREATE TABLE `ci_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=246 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_countries (245 records)
#
 
INSERT INTO `ci_countries` VALUES ('1', '+93', 'Afghanistan') ; 
INSERT INTO `ci_countries` VALUES ('2', '+355', 'Albania') ; 
INSERT INTO `ci_countries` VALUES ('3', 'DZ', 'Algeria') ; 
INSERT INTO `ci_countries` VALUES ('4', 'DS', 'American Samoa') ; 
INSERT INTO `ci_countries` VALUES ('5', 'AD', 'Andorra') ; 
INSERT INTO `ci_countries` VALUES ('6', 'AO', 'Angola') ; 
INSERT INTO `ci_countries` VALUES ('7', 'AI', 'Anguilla') ; 
INSERT INTO `ci_countries` VALUES ('8', 'AQ', 'Antarctica') ; 
INSERT INTO `ci_countries` VALUES ('9', 'AG', 'Antigua and Barbuda') ; 
INSERT INTO `ci_countries` VALUES ('10', 'AR', 'Argentina') ; 
INSERT INTO `ci_countries` VALUES ('11', 'AM', 'Armenia') ; 
INSERT INTO `ci_countries` VALUES ('12', 'AW', 'Aruba') ; 
INSERT INTO `ci_countries` VALUES ('13', 'AU', 'Australia') ; 
INSERT INTO `ci_countries` VALUES ('14', 'AT', 'Austria') ; 
INSERT INTO `ci_countries` VALUES ('15', 'AZ', 'Azerbaijan') ; 
INSERT INTO `ci_countries` VALUES ('16', 'BS', 'Bahamas') ; 
INSERT INTO `ci_countries` VALUES ('17', 'BH', 'Bahrain') ; 
INSERT INTO `ci_countries` VALUES ('18', 'BD', 'Bangladesh') ; 
INSERT INTO `ci_countries` VALUES ('19', 'BB', 'Barbados') ; 
INSERT INTO `ci_countries` VALUES ('20', 'BY', 'Belarus') ; 
INSERT INTO `ci_countries` VALUES ('21', 'BE', 'Belgium') ; 
INSERT INTO `ci_countries` VALUES ('22', 'BZ', 'Belize') ; 
INSERT INTO `ci_countries` VALUES ('23', 'BJ', 'Benin') ; 
INSERT INTO `ci_countries` VALUES ('24', 'BM', 'Bermuda') ; 
INSERT INTO `ci_countries` VALUES ('25', 'BT', 'Bhutan') ; 
INSERT INTO `ci_countries` VALUES ('26', 'BO', 'Bolivia') ; 
INSERT INTO `ci_countries` VALUES ('27', 'BA', 'Bosnia and Herzegovina') ; 
INSERT INTO `ci_countries` VALUES ('28', 'BW', 'Botswana') ; 
INSERT INTO `ci_countries` VALUES ('29', 'BV', 'Bouvet Island') ; 
INSERT INTO `ci_countries` VALUES ('30', 'BR', 'Brazil') ; 
INSERT INTO `ci_countries` VALUES ('31', 'IO', 'British Indian Ocean Territory') ; 
INSERT INTO `ci_countries` VALUES ('32', 'BN', 'Brunei Darussalam') ; 
INSERT INTO `ci_countries` VALUES ('33', 'BG', 'Bulgaria') ; 
INSERT INTO `ci_countries` VALUES ('34', 'BF', 'Burkina Faso') ; 
INSERT INTO `ci_countries` VALUES ('35', 'BI', 'Burundi') ; 
INSERT INTO `ci_countries` VALUES ('36', 'KH', 'Cambodia') ; 
INSERT INTO `ci_countries` VALUES ('37', 'CM', 'Cameroon') ; 
INSERT INTO `ci_countries` VALUES ('38', 'CA', 'Canada') ; 
INSERT INTO `ci_countries` VALUES ('39', 'CV', 'Cape Verde') ; 
INSERT INTO `ci_countries` VALUES ('40', 'KY', 'Cayman Islands') ; 
INSERT INTO `ci_countries` VALUES ('41', 'CF', 'Central African Republic') ; 
INSERT INTO `ci_countries` VALUES ('42', 'TD', 'Chad') ; 
INSERT INTO `ci_countries` VALUES ('43', 'CL', 'Chile') ; 
INSERT INTO `ci_countries` VALUES ('44', 'CN', 'China') ; 
INSERT INTO `ci_countries` VALUES ('45', 'CX', 'Christmas Island') ; 
INSERT INTO `ci_countries` VALUES ('46', 'CC', 'Cocos (Keeling) Islands') ; 
INSERT INTO `ci_countries` VALUES ('47', 'CO', 'Colombia') ; 
INSERT INTO `ci_countries` VALUES ('48', 'KM', 'Comoros') ; 
INSERT INTO `ci_countries` VALUES ('49', 'CG', 'Congo') ; 
INSERT INTO `ci_countries` VALUES ('50', 'CK', 'Cook Islands') ; 
INSERT INTO `ci_countries` VALUES ('51', 'CR', 'Costa Rica') ; 
INSERT INTO `ci_countries` VALUES ('52', 'HR', 'Croatia (Hrvatska)') ; 
INSERT INTO `ci_countries` VALUES ('53', 'CU', 'Cuba') ; 
INSERT INTO `ci_countries` VALUES ('54', 'CY', 'Cyprus') ; 
INSERT INTO `ci_countries` VALUES ('55', 'CZ', 'Czech Republic') ; 
INSERT INTO `ci_countries` VALUES ('56', 'DK', 'Denmark') ; 
INSERT INTO `ci_countries` VALUES ('57', 'DJ', 'Djibouti') ; 
INSERT INTO `ci_countries` VALUES ('58', 'DM', 'Dominica') ; 
INSERT INTO `ci_countries` VALUES ('59', 'DO', 'Dominican Republic') ; 
INSERT INTO `ci_countries` VALUES ('60', 'TP', 'East Timor') ; 
INSERT INTO `ci_countries` VALUES ('61', 'EC', 'Ecuador') ; 
INSERT INTO `ci_countries` VALUES ('62', 'EG', 'Egypt') ; 
INSERT INTO `ci_countries` VALUES ('63', 'SV', 'El Salvador') ; 
INSERT INTO `ci_countries` VALUES ('64', 'GQ', 'Equatorial Guinea') ; 
INSERT INTO `ci_countries` VALUES ('65', 'ER', 'Eritrea') ; 
INSERT INTO `ci_countries` VALUES ('66', 'EE', 'Estonia') ; 
INSERT INTO `ci_countries` VALUES ('67', 'ET', 'Ethiopia') ; 
INSERT INTO `ci_countries` VALUES ('68', 'FK', 'Falkland Islands (Malvinas)') ; 
INSERT INTO `ci_countries` VALUES ('69', 'FO', 'Faroe Islands') ; 
INSERT INTO `ci_countries` VALUES ('70', 'FJ', 'Fiji') ; 
INSERT INTO `ci_countries` VALUES ('71', 'FI', 'Finland') ; 
INSERT INTO `ci_countries` VALUES ('72', 'FR', 'France') ; 
INSERT INTO `ci_countries` VALUES ('73', 'FX', 'France, Metropolitan') ; 
INSERT INTO `ci_countries` VALUES ('74', 'GF', 'French Guiana') ; 
INSERT INTO `ci_countries` VALUES ('75', 'PF', 'French Polynesia') ; 
INSERT INTO `ci_countries` VALUES ('76', 'TF', 'French Southern Territories') ; 
INSERT INTO `ci_countries` VALUES ('77', 'GA', 'Gabon') ; 
INSERT INTO `ci_countries` VALUES ('78', 'GM', 'Gambia') ; 
INSERT INTO `ci_countries` VALUES ('79', 'GE', 'Georgia') ; 
INSERT INTO `ci_countries` VALUES ('80', 'DE', 'Germany') ; 
INSERT INTO `ci_countries` VALUES ('81', 'GH', 'Ghana') ; 
INSERT INTO `ci_countries` VALUES ('82', 'GI', 'Gibraltar') ; 
INSERT INTO `ci_countries` VALUES ('83', 'GK', 'Guernsey') ; 
INSERT INTO `ci_countries` VALUES ('84', 'GR', 'Greece') ; 
INSERT INTO `ci_countries` VALUES ('85', 'GL', 'Greenland') ; 
INSERT INTO `ci_countries` VALUES ('86', 'GD', 'Grenada') ; 
INSERT INTO `ci_countries` VALUES ('87', 'GP', 'Guadeloupe') ; 
INSERT INTO `ci_countries` VALUES ('88', 'GU', 'Guam') ; 
INSERT INTO `ci_countries` VALUES ('89', 'GT', 'Guatemala') ; 
INSERT INTO `ci_countries` VALUES ('90', 'GN', 'Guinea') ; 
INSERT INTO `ci_countries` VALUES ('91', 'GW', 'Guinea-Bissau') ; 
INSERT INTO `ci_countries` VALUES ('92', 'GY', 'Guyana') ; 
INSERT INTO `ci_countries` VALUES ('93', 'HT', 'Haiti') ; 
INSERT INTO `ci_countries` VALUES ('94', 'HM', 'Heard and Mc Donald Islands') ; 
INSERT INTO `ci_countries` VALUES ('95', 'HN', 'Honduras') ; 
INSERT INTO `ci_countries` VALUES ('96', 'HK', 'Hong Kong') ; 
INSERT INTO `ci_countries` VALUES ('97', 'HU', 'Hungary') ; 
INSERT INTO `ci_countries` VALUES ('98', 'IS', 'Iceland') ; 
INSERT INTO `ci_countries` VALUES ('99', 'IN', 'India') ; 
INSERT INTO `ci_countries` VALUES ('100', 'IM', 'Isle of Man') ; 
INSERT INTO `ci_countries` VALUES ('101', 'ID', 'Indonesia') ; 
INSERT INTO `ci_countries` VALUES ('102', 'IR', 'Iran (Islamic Republic of)') ; 
INSERT INTO `ci_countries` VALUES ('103', 'IQ', 'Iraq') ; 
INSERT INTO `ci_countries` VALUES ('104', 'IE', 'Ireland') ; 
INSERT INTO `ci_countries` VALUES ('105', 'IL', 'Israel') ; 
INSERT INTO `ci_countries` VALUES ('106', 'IT', 'Italy') ; 
INSERT INTO `ci_countries` VALUES ('107', 'CI', 'Ivory Coast') ; 
INSERT INTO `ci_countries` VALUES ('108', 'JE', 'Jersey') ; 
INSERT INTO `ci_countries` VALUES ('109', 'JM', 'Jamaica') ; 
INSERT INTO `ci_countries` VALUES ('110', 'JP', 'Japan') ; 
INSERT INTO `ci_countries` VALUES ('111', 'JO', 'Jordan') ; 
INSERT INTO `ci_countries` VALUES ('112', 'KZ', 'Kazakhstan') ; 
INSERT INTO `ci_countries` VALUES ('113', 'KE', 'Kenya') ; 
INSERT INTO `ci_countries` VALUES ('114', 'KI', 'Kiribati') ; 
INSERT INTO `ci_countries` VALUES ('115', 'KP', 'Korea, Democratic People\'s Republic of') ; 
INSERT INTO `ci_countries` VALUES ('116', 'KR', 'Korea, Republic of') ; 
INSERT INTO `ci_countries` VALUES ('117', 'XK', 'Kosovo') ; 
INSERT INTO `ci_countries` VALUES ('118', 'KW', 'Kuwait') ; 
INSERT INTO `ci_countries` VALUES ('119', 'KG', 'Kyrgyzstan') ; 
INSERT INTO `ci_countries` VALUES ('120', 'LA', 'Lao People\'s Democratic Republic') ; 
INSERT INTO `ci_countries` VALUES ('121', 'LV', 'Latvia') ; 
INSERT INTO `ci_countries` VALUES ('122', 'LB', 'Lebanon') ; 
INSERT INTO `ci_countries` VALUES ('123', 'LS', 'Lesotho') ; 
INSERT INTO `ci_countries` VALUES ('124', 'LR', 'Liberia') ; 
INSERT INTO `ci_countries` VALUES ('125', 'LY', 'Libyan Arab Jamahiriya') ; 
INSERT INTO `ci_countries` VALUES ('126', 'LI', 'Liechtenstein') ; 
INSERT INTO `ci_countries` VALUES ('127', 'LT', 'Lithuania') ; 
INSERT INTO `ci_countries` VALUES ('128', 'LU', 'Luxembourg') ; 
INSERT INTO `ci_countries` VALUES ('129', 'MO', 'Macau') ; 
INSERT INTO `ci_countries` VALUES ('130', 'MK', 'Macedonia') ; 
INSERT INTO `ci_countries` VALUES ('131', 'MG', 'Madagascar') ; 
INSERT INTO `ci_countries` VALUES ('132', 'MW', 'Malawi') ; 
INSERT INTO `ci_countries` VALUES ('133', 'MY', 'Malaysia') ; 
INSERT INTO `ci_countries` VALUES ('134', 'MV', 'Maldives') ; 
INSERT INTO `ci_countries` VALUES ('135', 'ML', 'Mali') ; 
INSERT INTO `ci_countries` VALUES ('136', 'MT', 'Malta') ; 
INSERT INTO `ci_countries` VALUES ('137', 'MH', 'Marshall Islands') ; 
INSERT INTO `ci_countries` VALUES ('138', 'MQ', 'Martinique') ; 
INSERT INTO `ci_countries` VALUES ('139', 'MR', 'Mauritania') ; 
INSERT INTO `ci_countries` VALUES ('140', 'MU', 'Mauritius') ; 
INSERT INTO `ci_countries` VALUES ('141', 'TY', 'Mayotte') ; 
INSERT INTO `ci_countries` VALUES ('142', 'MX', 'Mexico') ; 
INSERT INTO `ci_countries` VALUES ('143', 'FM', 'Micronesia, Federated States of') ; 
INSERT INTO `ci_countries` VALUES ('144', 'MD', 'Moldova, Republic of') ; 
INSERT INTO `ci_countries` VALUES ('145', 'MC', 'Monaco') ; 
INSERT INTO `ci_countries` VALUES ('146', 'MN', 'Mongolia') ; 
INSERT INTO `ci_countries` VALUES ('147', 'ME', 'Montenegro') ; 
INSERT INTO `ci_countries` VALUES ('148', 'MS', 'Montserrat') ; 
INSERT INTO `ci_countries` VALUES ('149', 'MA', 'Morocco') ; 
INSERT INTO `ci_countries` VALUES ('150', 'MZ', 'Mozambique') ; 
INSERT INTO `ci_countries` VALUES ('151', 'MM', 'Myanmar') ; 
INSERT INTO `ci_countries` VALUES ('152', 'NA', 'Namibia') ; 
INSERT INTO `ci_countries` VALUES ('153', 'NR', 'Nauru') ; 
INSERT INTO `ci_countries` VALUES ('154', 'NP', 'Nepal') ; 
INSERT INTO `ci_countries` VALUES ('155', 'NL', 'Netherlands') ; 
INSERT INTO `ci_countries` VALUES ('156', 'AN', 'Netherlands Antilles') ; 
INSERT INTO `ci_countries` VALUES ('157', 'NC', 'New Caledonia') ; 
INSERT INTO `ci_countries` VALUES ('158', 'NZ', 'New Zealand') ; 
INSERT INTO `ci_countries` VALUES ('159', 'NI', 'Nicaragua') ; 
INSERT INTO `ci_countries` VALUES ('160', 'NE', 'Niger') ; 
INSERT INTO `ci_countries` VALUES ('161', 'NG', 'Nigeria') ; 
INSERT INTO `ci_countries` VALUES ('162', 'NU', 'Niue') ; 
INSERT INTO `ci_countries` VALUES ('163', 'NF', 'Norfolk Island') ; 
INSERT INTO `ci_countries` VALUES ('164', 'MP', 'Northern Mariana Islands') ; 
INSERT INTO `ci_countries` VALUES ('165', 'NO', 'Norway') ; 
INSERT INTO `ci_countries` VALUES ('166', 'OM', 'Oman') ; 
INSERT INTO `ci_countries` VALUES ('167', 'PK', 'Pakistan') ; 
INSERT INTO `ci_countries` VALUES ('168', 'PW', 'Palau') ; 
INSERT INTO `ci_countries` VALUES ('169', 'PS', 'Palestine') ; 
INSERT INTO `ci_countries` VALUES ('170', 'PA', 'Panama') ; 
INSERT INTO `ci_countries` VALUES ('171', 'PG', 'Papua New Guinea') ; 
INSERT INTO `ci_countries` VALUES ('172', 'PY', 'Paraguay') ; 
INSERT INTO `ci_countries` VALUES ('173', 'PE', 'Peru') ; 
INSERT INTO `ci_countries` VALUES ('174', 'PH', 'Philippines') ; 
INSERT INTO `ci_countries` VALUES ('175', 'PN', 'Pitcairn') ; 
INSERT INTO `ci_countries` VALUES ('176', 'PL', 'Poland') ; 
INSERT INTO `ci_countries` VALUES ('177', 'PT', 'Portugal') ; 
INSERT INTO `ci_countries` VALUES ('178', 'PR', 'Puerto Rico') ; 
INSERT INTO `ci_countries` VALUES ('179', 'QA', 'Qatar') ; 
INSERT INTO `ci_countries` VALUES ('180', 'RE', 'Reunion') ; 
INSERT INTO `ci_countries` VALUES ('181', 'RO', 'Romania') ; 
INSERT INTO `ci_countries` VALUES ('182', 'RU', 'Russian Federation') ; 
INSERT INTO `ci_countries` VALUES ('183', 'RW', 'Rwanda') ; 
INSERT INTO `ci_countries` VALUES ('184', 'KN', 'Saint Kitts and Nevis') ; 
INSERT INTO `ci_countries` VALUES ('185', 'LC', 'Saint Lucia') ; 
INSERT INTO `ci_countries` VALUES ('186', 'VC', 'Saint Vincent and the Grenadines') ; 
INSERT INTO `ci_countries` VALUES ('187', 'WS', 'Samoa') ; 
INSERT INTO `ci_countries` VALUES ('188', 'SM', 'San Marino') ; 
INSERT INTO `ci_countries` VALUES ('189', 'ST', 'Sao Tome and Principe') ; 
INSERT INTO `ci_countries` VALUES ('190', 'SA', 'Saudi Arabia') ; 
INSERT INTO `ci_countries` VALUES ('191', 'SN', 'Senegal') ; 
INSERT INTO `ci_countries` VALUES ('192', 'RS', 'Serbia') ; 
INSERT INTO `ci_countries` VALUES ('193', 'SC', 'Seychelles') ; 
INSERT INTO `ci_countries` VALUES ('194', 'SL', 'Sierra Leone') ; 
INSERT INTO `ci_countries` VALUES ('195', 'SG', 'Singapore') ; 
INSERT INTO `ci_countries` VALUES ('196', 'SK', 'Slovakia') ; 
INSERT INTO `ci_countries` VALUES ('197', 'SI', 'Slovenia') ; 
INSERT INTO `ci_countries` VALUES ('198', 'SB', 'Solomon Islands') ; 
INSERT INTO `ci_countries` VALUES ('199', 'SO', 'Somalia') ; 
INSERT INTO `ci_countries` VALUES ('200', 'ZA', 'South Africa') ; 
INSERT INTO `ci_countries` VALUES ('201', 'GS', 'South Georgia South Sandwich Islands') ; 
INSERT INTO `ci_countries` VALUES ('202', 'ES', 'Spain') ; 
INSERT INTO `ci_countries` VALUES ('203', 'LK', 'Sri Lanka') ; 
INSERT INTO `ci_countries` VALUES ('204', 'SH', 'St. Helena') ; 
INSERT INTO `ci_countries` VALUES ('205', 'PM', 'St. Pierre and Miquelon') ; 
INSERT INTO `ci_countries` VALUES ('206', 'SD', 'Sudan') ; 
INSERT INTO `ci_countries` VALUES ('207', 'SR', 'Suriname') ; 
INSERT INTO `ci_countries` VALUES ('208', 'SJ', 'Svalbard and Jan Mayen Islands') ; 
INSERT INTO `ci_countries` VALUES ('209', 'SZ', 'Swaziland') ; 
INSERT INTO `ci_countries` VALUES ('210', 'SE', 'Sweden') ; 
INSERT INTO `ci_countries` VALUES ('211', 'CH', 'Switzerland') ; 
INSERT INTO `ci_countries` VALUES ('212', 'SY', 'Syrian Arab Republic') ; 
INSERT INTO `ci_countries` VALUES ('213', 'TW', 'Taiwan') ; 
INSERT INTO `ci_countries` VALUES ('214', 'TJ', 'Tajikistan') ; 
INSERT INTO `ci_countries` VALUES ('215', 'TZ', 'Tanzania, United Republic of') ; 
INSERT INTO `ci_countries` VALUES ('216', 'TH', 'Thailand') ; 
INSERT INTO `ci_countries` VALUES ('217', 'TG', 'Togo') ; 
INSERT INTO `ci_countries` VALUES ('218', 'TK', 'Tokelau') ; 
INSERT INTO `ci_countries` VALUES ('219', 'TO', 'Tonga') ; 
INSERT INTO `ci_countries` VALUES ('220', 'TT', 'Trinidad and Tobago') ; 
INSERT INTO `ci_countries` VALUES ('221', 'TN', 'Tunisia') ; 
INSERT INTO `ci_countries` VALUES ('222', 'TR', 'Turkey') ; 
INSERT INTO `ci_countries` VALUES ('223', 'TM', 'Turkmenistan') ; 
INSERT INTO `ci_countries` VALUES ('224', 'TC', 'Turks and Caicos Islands') ; 
INSERT INTO `ci_countries` VALUES ('225', 'TV', 'Tuvalu') ; 
INSERT INTO `ci_countries` VALUES ('226', 'UG', 'Uganda') ; 
INSERT INTO `ci_countries` VALUES ('227', 'UA', 'Ukraine') ; 
INSERT INTO `ci_countries` VALUES ('228', 'AE', 'United Arab Emirates') ; 
INSERT INTO `ci_countries` VALUES ('229', 'GB', 'United Kingdom') ; 
INSERT INTO `ci_countries` VALUES ('230', 'US', 'United States') ; 
INSERT INTO `ci_countries` VALUES ('231', 'UM', 'United States minor outlying islands') ; 
INSERT INTO `ci_countries` VALUES ('232', 'UY', 'Uruguay') ; 
INSERT INTO `ci_countries` VALUES ('233', 'UZ', 'Uzbekistan') ; 
INSERT INTO `ci_countries` VALUES ('234', 'VU', 'Vanuatu') ; 
INSERT INTO `ci_countries` VALUES ('235', 'VA', 'Vatican City State') ; 
INSERT INTO `ci_countries` VALUES ('236', 'VE', 'Venezuela') ; 
INSERT INTO `ci_countries` VALUES ('237', 'VN', 'Vietnam') ; 
INSERT INTO `ci_countries` VALUES ('238', 'VG', 'Virgin Islands (British)') ; 
INSERT INTO `ci_countries` VALUES ('239', 'VI', 'Virgin Islands (U.S.)') ; 
INSERT INTO `ci_countries` VALUES ('240', 'WF', 'Wallis and Futuna Islands') ; 
INSERT INTO `ci_countries` VALUES ('241', 'EH', 'Western Sahara') ; 
INSERT INTO `ci_countries` VALUES ('242', 'YE', 'Yemen') ; 
INSERT INTO `ci_countries` VALUES ('243', 'ZR', 'Zaire') ; 
INSERT INTO `ci_countries` VALUES ('244', 'ZM', 'Zambia') ; 
INSERT INTO `ci_countries` VALUES ('245', 'ZW', 'Zimbabwe') ;
#
# End of data contents of table ci_countries
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------


#
# Delete any existing table `ci_database_backup`
#

DROP TABLE IF EXISTS `ci_database_backup`;


#
# Table structure of table `ci_database_backup`
#

CREATE TABLE `ci_database_backup` (
  `backup_id` int(111) NOT NULL AUTO_INCREMENT,
  `backup_file` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`backup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_database_backup (0 records)
#

#
# End of data contents of table ci_database_backup
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------


#
# Delete any existing table `ci_departments`
#

DROP TABLE IF EXISTS `ci_departments`;


#
# Table structure of table `ci_departments`
#

CREATE TABLE `ci_departments` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `department_head` int(11) DEFAULT '0',
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_departments (4 records)
#
 
INSERT INTO `ci_departments` VALUES ('8', 'IT Management', '2', '24', '2', '21-09-2021 09:34:20') ; 
INSERT INTO `ci_departments` VALUES ('9', 'Accounting and Taxation', '2', '3', '2', '21-09-2021 09:36:21') ; 
INSERT INTO `ci_departments` VALUES ('10', 'Pro Tax Team', '2', '4', '2', '21-09-2021 09:36:37') ; 
INSERT INTO `ci_departments` VALUES ('11', 'Auditing Team', '2', '5', '2', '21-09-2021 09:36:48') ;
#
# End of data contents of table ci_departments
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------


#
# Delete any existing table `ci_designations`
#

DROP TABLE IF EXISTS `ci_designations`;


#
# Table structure of table `ci_designations`
#

CREATE TABLE `ci_designations` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `designation_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`designation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_designations (10 records)
#
 
INSERT INTO `ci_designations` VALUES ('1', '9', '2', 'Junior Accountant', '', '20-07-2021 05:36:47') ; 
INSERT INTO `ci_designations` VALUES ('2', '9', '2', 'Senior Accountant', '', '20-07-2021 05:37:09') ; 
INSERT INTO `ci_designations` VALUES ('4', '8', '2', 'Junior Developer', '', '20-07-2021 05:37:47') ; 
INSERT INTO `ci_designations` VALUES ('5', '8', '2', 'Senior Developer', '', '20-07-2021 05:37:59') ; 
INSERT INTO `ci_designations` VALUES ('6', '5', '2', 'General Manager', '', '20-07-2021 05:39:11') ; 
INSERT INTO `ci_designations` VALUES ('7', '8', '2', 'MIS - Reporting', 'MIS - management reporting systems.', '21-09-2021 09:26:56') ; 
INSERT INTO `ci_designations` VALUES ('8', '10', '2', 'Junior Accountant', '', '21-09-2021 09:41:25') ; 
INSERT INTO `ci_designations` VALUES ('9', '11', '2', 'Junior Accountant', '', '21-09-2021 09:59:46') ; 
INSERT INTO `ci_designations` VALUES ('10', '9', '2', 'Receptionist', '', '21-09-2021 11:54:03') ; 
INSERT INTO `ci_designations` VALUES ('11', '8', '2', 'IT Manager', '', '22-09-2021 07:19:26') ;
#
# End of data contents of table ci_designations
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------


#
# Delete any existing table `ci_email_template`
#

DROP TABLE IF EXISTS `ci_email_template`;


#
# Table structure of table `ci_email_template`
#

CREATE TABLE `ci_email_template` (
  `template_id` int(111) NOT NULL AUTO_INCREMENT,
  `template_code` varchar(255) NOT NULL,
  `template_type` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_email_template (13 records)
#
 
INSERT INTO `ci_email_template` VALUES ('1', 'code1', 'super_admin', 'Forgot Password', 'Forgot Password', '&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;There was recently a request for password for your {site_name} account.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;To reset password, visit the following link&lt;/span&gt;&amp;nbsp;&lt;a href=\"{site_url}erp/verified-password?v={user_id}\" title=\"Reset Password\" target=\"_blank\"&gt;&lt;strong&gt;&lt;span style=\"forced-color-adjust:none;color:#6699cc;\"&gt;Reset Password&lt;/span&gt;&lt;/strong&gt;&lt;/a&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;If this was a mistake, just ignore this email and nothing will happen.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('2', 'code1', 'super_admin', 'Password Changed Successfully', 'Password Changed Successfully', '&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Congratulations! Your password has been updated successfully.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Your Username is: {username}&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Your new password is: {password}&lt;/span&gt;&lt;br /&gt;&lt;/p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;{site_name}&lt;/span&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('5', 'code1', 'super_admin', 'Add New Employee|Client|SuperAdmin User', 'Warm Welcome', '&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;box-sizing:border-box;\"&gt;&lt;/span&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Welcome to {site_name}. We have listed your sign-in details below, please make sure you keep them safe.&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Your Username is: {user_username}&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Your Password is: {user_password}&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;a href=\"{site_url}erp/login\" title=\"Login Here\" target=\"_blank\"&gt;Login Here&lt;/a&gt;&lt;/p&gt;&lt;p class=\"mt-4\" style=\"margin-bottom:0px;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;margin-top:1.5rem !important;\"&gt;----&lt;br style=\"box-sizing:border-box;\" /&gt;Thank You&lt;br style=\"box-sizing:border-box;\" /&gt;{site_name}&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('8', 'code1', 'super_admin', 'Contact Us | From Frontend', 'Contact Us', '&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;box-sizing:border-box;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;This email was sent through your support contact form on {site_name}.&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;/span&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Sender: {full_name}&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;Subject: {subject}&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;Email: {email}&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Message:&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;{message}&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;You can reply directly to this email to respond to {full_name}&lt;/p&gt;&lt;p class=\"mt-4\" style=\"margin-bottom:0px;box-sizing:border-box;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;margin-top:1.5rem !important;\"&gt;----&lt;br style=\"box-sizing:border-box;\" /&gt;Thank You&lt;br style=\"box-sizing:border-box;\" /&gt;{&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;site_name&lt;/span&gt;}&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('9', 'code1', 'super_admin', 'New Project Assigned', 'New Project Assigned', '&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;New project has been assigned to you.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Project Name: {project_name}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Project Due Date: {project_due_date}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('10', 'code1', 'super_admin', 'New Task Assigned', 'New Task Assigned', '&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;New task has been assigned to you.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Task Name: {task_name}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Task Due Date: {task_due_date}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('11', 'code1', 'super_admin', 'New Award', 'Award Received', '&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;You have been awarded with &lt;span style=\"text-decoration:underline;\"&gt;{award_name}&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;You can view this award by logging into the portal.&lt;/span&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('12', 'code1', 'super_admin', 'New Ticket Inquiry', 'New Inquiry [#{ticket_code}]', '&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;You have received a new inquiry&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Inquiry code: &lt;span style=\"text-decoration:underline;\"&gt;&lt;strong&gt;#{ticket_code}&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;You can view this &lt;/span&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;inquiry&amp;nbsp;&lt;/span&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;by logging into the portal.&lt;/span&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('13', 'code1', 'super_admin', 'New Leave Requested | For Company', 'New Leave Request', '&lt;p style=\"box-sizing:border-box;margin-bottom:1rem;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p style=\"box-sizing:border-box;margin-bottom:1rem;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;span style=\"text-decoration:underline;\"&gt;&lt;strong&gt;{employee_name}&lt;/strong&gt;&lt;/span&gt; wants a leave &lt;strong&gt;&lt;span style=\"text-decoration:underline;\"&gt;{leave_type}&lt;/span&gt;&lt;/strong&gt; from you. You can view the leave details by logging into the portal.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"box-sizing:border-box;margin-bottom:1rem;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('14', 'code1', 'super_admin', 'Leave Approved | For Employee', 'Your Leave Approved', '&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Congratulations! Your leave &lt;strong&gt;&lt;span style=\"text-decoration:underline;\"&gt;{leave_type}&lt;/span&gt;&lt;/strong&gt; request from {start_date} to {end_date} has been approved by your company management.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, Helvetica Neue, Arial, Noto Sans, sans-serif;\"&gt;&lt;span style=\"font-size:14px;\"&gt;Remarks:&lt;/span&gt;&lt;/span&gt;&lt;br style=\"font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-indent:0px;text-transform:none;white-space:normal;widows:2;word-spacing:0px;-webkit-text-stroke-width:0px;text-decoration-thickness:initial;text-decoration-style:initial;text-decoration-color:initial;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-indent:0px;text-transform:none;white-space:normal;widows:2;word-spacing:0px;-webkit-text-stroke-width:0px;text-decoration-thickness:initial;text-decoration-style:initial;text-decoration-color:initial;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{remarks}&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;You can view the leave details by logging into the portal.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('15', 'code1', 'super_admin', 'Leave Rejected | For Employee', 'Your Leave Rejected', '&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Unfortunately! Your leave&amp;nbsp;&lt;strong&gt;&lt;span style=\"text-decoration-line:underline;\"&gt;{leave_type}&lt;/span&gt;&lt;/strong&gt;&amp;nbsp;request from {start_date} to {end_date} has been rejected by your company management.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, Helvetica Neue, Arial, Noto Sans, sans-serif;\"&gt;&lt;span style=\"font-size:14px;\"&gt;Reject Reason:&lt;/span&gt;&lt;/span&gt;&lt;br style=\"font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-indent:0px;text-transform:none;white-space:normal;widows:2;word-spacing:0px;-webkit-text-stroke-width:0px;text-decoration-thickness:initial;text-decoration-style:initial;text-decoration-color:initial;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:start;text-indent:0px;text-transform:none;white-space:normal;widows:2;word-spacing:0px;-webkit-text-stroke-width:0px;text-decoration-thickness:initial;text-decoration-style:initial;text-decoration-color:initial;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{remarks}&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;You can view the leave details by logging into the portal.&lt;/span&gt;&lt;/p&gt;&lt;p style=\"margin-bottom:1rem;box-sizing:border-box;color:#4e5155;font-family:Roboto, -apple-system, BlinkMacSystemFont, \'Segoe UI\', Oxygen, Ubuntu, Cantarell, \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;font-size:14.304px;background-color:#ffffff;\"&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('16', 'code1', 'super_admin', 'New Job Posted | For Employee', 'New Job Posted', '&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;We would like to announce a new vacancy for a &lt;strong&gt;&lt;span style=\"text-decoration:underline;\"&gt;{job_title}&lt;/span&gt;&lt;/strong&gt;.&lt;/span&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Suitable applicants can send submit their resumes before &lt;span style=\"text-decoration:underline;\"&gt;&lt;strong&gt;{closing_date}&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;You can view a complete job description by logging into the portal.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{site_name}&lt;/span&gt;&lt;/p&gt;', '1') ; 
INSERT INTO `ci_email_template` VALUES ('17', 'code1', 'super_admin', 'Payslip Created | For Employee', 'Salary Slip for {month_year}', '&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;Hi There,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;New Payslip is created for {month_year}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;You can view this payslip by logging into the portal.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;background-color:#ffffff;\"&gt;if you have any question, do not hesitate to contact your HR Department.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;----&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;Thank You,&lt;/span&gt;&lt;br style=\"color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;box-sizing:border-box;\" /&gt;&lt;span style=\"forced-color-adjust:none;color:#8094ae;font-family:Roboto, sans-serif, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif;font-size:14px;\"&gt;{company_name}&lt;/span&gt;&lt;/p&gt;', '1') ;
#
# End of data contents of table ci_email_template
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------


#
# Delete any existing table `ci_employee_contacts`
#

DROP TABLE IF EXISTS `ci_employee_contacts`;


#
# Table structure of table `ci_employee_contacts`
#

CREATE TABLE `ci_employee_contacts` (
  `contact_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `is_primary` int(111) DEFAULT NULL,
  `is_dependent` int(111) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `work_phone` varchar(255) DEFAULT NULL,
  `work_phone_extension` varchar(255) DEFAULT NULL,
  `mobile_phone` varchar(255) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `work_email` varchar(255) DEFAULT NULL,
  `personal_email` varchar(255) DEFAULT NULL,
  `address_1` mediumtext,
  `address_2` mediumtext,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_employee_contacts (0 records)
#

#
# End of data contents of table ci_employee_contacts
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------


#
# Delete any existing table `ci_employee_exit`
#

DROP TABLE IF EXISTS `ci_employee_exit`;


#
# Table structure of table `ci_employee_exit`
#

CREATE TABLE `ci_employee_exit` (
  `exit_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `exit_date` varchar(255) NOT NULL,
  `exit_type_id` int(111) NOT NULL,
  `exit_interview` int(111) NOT NULL,
  `is_inactivate_account` int(111) NOT NULL,
  `reason` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`exit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_employee_exit (0 records)
#

#
# End of data contents of table ci_employee_exit
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------


#
# Delete any existing table `ci_erp_company_settings`
#

DROP TABLE IF EXISTS `ci_erp_company_settings`;


#
# Table structure of table `ci_erp_company_settings`
#

CREATE TABLE `ci_erp_company_settings` (
  `setting_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `default_currency` varchar(255) NOT NULL DEFAULT 'USD',
  `default_currency_symbol` varchar(100) NOT NULL DEFAULT 'USD',
  `notification_position` varchar(255) DEFAULT NULL,
  `notification_close_btn` varchar(255) DEFAULT NULL,
  `notification_bar` varchar(255) DEFAULT NULL,
  `date_format_xi` varchar(255) DEFAULT NULL,
  `default_language` varchar(200) NOT NULL DEFAULT 'en',
  `system_timezone` varchar(200) NOT NULL DEFAULT 'Asia/Bishkek',
  `paypal_email` varchar(100) DEFAULT NULL,
  `paypal_sandbox` varchar(10) DEFAULT NULL,
  `paypal_active` varchar(10) DEFAULT NULL,
  `stripe_secret_key` varchar(200) DEFAULT NULL,
  `stripe_publishable_key` varchar(200) DEFAULT NULL,
  `stripe_active` varchar(10) DEFAULT NULL,
  `invoice_terms_condition` text,
  `updated_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_erp_company_settings (1 records)
#
 
INSERT INTO `ci_erp_company_settings` VALUES ('1', '2', 'INR', 'INR', 'toast-top-center', '0', 'true', 'Y.m.d', 'en', 'Asia/Kolkata', 'paypal@example.com', 'yes', 'yes', 'stripe_secret_key', 'stripe_publishable_key', 'yes', 'ABYZ IT & CALL SERVICES PVT LTD', '15-05-2021 08:11:26') ;
#
# End of data contents of table ci_erp_company_settings
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------


#
# Delete any existing table `ci_erp_constants`
#

DROP TABLE IF EXISTS `ci_erp_constants`;


#
# Table structure of table `ci_erp_constants`
#

CREATE TABLE `ci_erp_constants` (
  `constants_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `field_one` varchar(200) DEFAULT NULL,
  `field_two` varchar(200) DEFAULT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`constants_id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_erp_constants (103 records)
#
 
INSERT INTO `ci_erp_constants` VALUES ('3', '81', 'company_type', 'Corporation', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('4', '81', 'company_type', 'Exempt Organization', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('5', '81', 'company_type', 'Partnership', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('6', '81', 'company_type', 'Private Foundation', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('7', '81', 'company_type', 'Limited Liability Company', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('16', '81', 'religion', 'Agnosticism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('17', '81', 'religion', 'Atheism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('18', '81', 'religion', 'Baha\'i', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('19', '81', 'religion', 'Buddhism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('20', '81', 'religion', 'Christianity', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('21', '81', 'religion', 'Humanism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('22', '81', 'religion', 'Hinduism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('23', '81', 'religion', 'Islam', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('24', '81', 'religion', 'Jainism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('25', '81', 'religion', 'Judaism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('26', '81', 'religion', 'Sikhism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('27', '81', 'religion', 'Zoroastrianism', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('93', '81', 'payment_method', 'Cash', '', '', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('94', '81', 'payment_method', 'Paypal', '', '', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('95', '81', 'payment_method', 'Bank', '', '', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('98', '81', 'payment_method', 'Stripe', '', '', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('99', '81', 'payment_method', 'Paystack', '', '', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('100', '81', 'payment_method', 'Cheque', '', '', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('101', '2', 'competencies', 'Leadership', 'Null', 'Null', '15-05-2021 05:37:16') ; 
INSERT INTO `ci_erp_constants` VALUES ('102', '2', 'competencies', 'Professional Impact', 'Null', 'Null', '15-05-2021 05:37:59') ; 
INSERT INTO `ci_erp_constants` VALUES ('103', '2', 'competencies', 'Oral Communication', 'Null', 'Null', '15-05-2021 05:38:48') ; 
INSERT INTO `ci_erp_constants` VALUES ('104', '2', 'competencies', 'Self Management', 'Null', 'Null', '15-05-2021 05:39:03') ; 
INSERT INTO `ci_erp_constants` VALUES ('105', '2', 'competencies', 'Team Work', 'Null', 'Null', '15-05-2021 05:39:45') ; 
INSERT INTO `ci_erp_constants` VALUES ('106', '2', 'competencies2', 'Allocating Resources', 'Null', 'Null', '15-05-2021 05:40:05') ; 
INSERT INTO `ci_erp_constants` VALUES ('107', '2', 'competencies2', 'Organizational Design', 'Null', 'Null', '15-05-2021 05:40:24') ; 
INSERT INTO `ci_erp_constants` VALUES ('108', '2', 'competencies2', 'Organizational Savvy', 'Null', 'Null', '15-05-2021 05:40:28') ; 
INSERT INTO `ci_erp_constants` VALUES ('109', '2', 'competencies2', 'Business Process', 'Null', 'Null', '15-05-2021 05:40:40') ; 
INSERT INTO `ci_erp_constants` VALUES ('110', '2', 'competencies2', 'Project Management', 'Null', 'Null', '15-05-2021 05:40:49') ; 
INSERT INTO `ci_erp_constants` VALUES ('111', '2', 'tax_type', 'Capital Gains', '10', 'percentage', '15-05-2021 02:14:32') ; 
INSERT INTO `ci_erp_constants` VALUES ('112', '2', 'tax_type', 'Value-Added Tax', '5', 'percentage', '15-05-2021 02:15:08') ; 
INSERT INTO `ci_erp_constants` VALUES ('113', '2', 'tax_type', 'Excise Taxes', '12', 'fixed', '15-05-2021 02:15:37') ; 
INSERT INTO `ci_erp_constants` VALUES ('114', '2', 'tax_type', 'Wealth Taxes', '8', 'percentage', '15-05-2021 02:16:02') ; 
INSERT INTO `ci_erp_constants` VALUES ('115', '2', 'tax_type', 'No Tax', '0', 'fixed', '15-05-2021 02:16:28') ; 
INSERT INTO `ci_erp_constants` VALUES ('116', '2', 'leave_type', 'Annual', '10', '1', '15-05-2021 03:23:35') ; 
INSERT INTO `ci_erp_constants` VALUES ('117', '2', 'leave_type', 'Sick', '5', '1', '15-05-2021 03:23:45') ; 
INSERT INTO `ci_erp_constants` VALUES ('118', '2', 'leave_type', 'Hospitalisation', '2', '1', '15-05-2021 03:23:56') ; 
INSERT INTO `ci_erp_constants` VALUES ('119', '2', 'leave_type', 'Maternity', '7', '1', '15-05-2021 03:24:09') ; 
INSERT INTO `ci_erp_constants` VALUES ('120', '2', 'leave_type', 'Paternity', '7', '1', '15-05-2021 03:24:19') ; 
INSERT INTO `ci_erp_constants` VALUES ('121', '2', 'leave_type', 'LOP', '10', '1', '15-05-2021 03:24:26') ; 
INSERT INTO `ci_erp_constants` VALUES ('122', '2', 'leave_type', 'Bereavement', '10', '1', '15-05-2021 03:27:02') ; 
INSERT INTO `ci_erp_constants` VALUES ('123', '2', 'leave_type', 'Compensatory', '5', '1', '15-05-2021 03:27:18') ; 
INSERT INTO `ci_erp_constants` VALUES ('124', '2', 'leave_type', 'Sabbatical', '7', '1', '15-05-2021 03:27:32') ; 
INSERT INTO `ci_erp_constants` VALUES ('125', '2', 'training_type', 'Technical', 'Null', 'Null', '15-05-2021 03:35:04') ; 
INSERT INTO `ci_erp_constants` VALUES ('126', '2', 'training_type', 'Advanced research skills', 'Null', 'Null', '15-05-2021 03:35:16') ; 
INSERT INTO `ci_erp_constants` VALUES ('127', '2', 'training_type', 'Strong communication skills', 'Null', 'Null', '15-05-2021 03:35:25') ; 
INSERT INTO `ci_erp_constants` VALUES ('128', '2', 'training_type', 'Adaptability skills', 'Null', 'Null', '15-05-2021 03:35:33') ; 
INSERT INTO `ci_erp_constants` VALUES ('129', '2', 'training_type', 'Social media', 'Null', 'Null', '15-05-2021 03:35:47') ; 
INSERT INTO `ci_erp_constants` VALUES ('130', '2', 'training_type', 'Enthusiasm for Learning', 'Null', 'Null', '15-05-2021 03:36:01') ; 
INSERT INTO `ci_erp_constants` VALUES ('131', '2', 'training_type', 'Soft Skills', 'Null', 'Null', '15-05-2021 03:36:09') ; 
INSERT INTO `ci_erp_constants` VALUES ('132', '2', 'training_type', 'Professional Training', 'Null', 'Null', '15-05-2021 03:36:16') ; 
INSERT INTO `ci_erp_constants` VALUES ('133', '2', 'training_type', 'Team Training', 'Null', 'Null', '15-05-2021 03:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('134', '2', 'goal_type', 'Revamp Employee experience', 'Null', 'Null', '15-05-2021 03:42:46') ; 
INSERT INTO `ci_erp_constants` VALUES ('135', '2', 'goal_type', 'Talent Retention', 'Null', 'Null', '15-05-2021 03:42:55') ; 
INSERT INTO `ci_erp_constants` VALUES ('136', '2', 'goal_type', 'Talent Acquisition', 'Null', 'Null', '15-05-2021 03:43:09') ; 
INSERT INTO `ci_erp_constants` VALUES ('137', '2', 'goal_type', 'Strengthen the Feedback Structure', 'Null', 'Null', '15-05-2021 04:16:26') ; 
INSERT INTO `ci_erp_constants` VALUES ('138', '2', 'goal_type', 'Boost Company culture', 'Null', 'Null', '15-05-2021 04:17:47') ; 
INSERT INTO `ci_erp_constants` VALUES ('139', '2', 'warning_type', 'Written Notice', 'Null', 'Null', '15-05-2021 04:33:13') ; 
INSERT INTO `ci_erp_constants` VALUES ('140', '2', 'warning_type', 'Letter of Written Reprimand', 'Null', 'Null', '15-05-2021 04:33:25') ; 
INSERT INTO `ci_erp_constants` VALUES ('141', '2', 'warning_type', 'Letter of Suspension', 'Null', 'Null', '15-05-2021 04:33:33') ; 
INSERT INTO `ci_erp_constants` VALUES ('142', '2', 'warning_type', 'Disciplinary Demotion', 'Null', 'Null', '15-05-2021 04:33:40') ; 
INSERT INTO `ci_erp_constants` VALUES ('143', '2', 'warning_type', 'Letter of Discharge', 'Null', 'Null', '15-05-2021 04:34:06') ; 
INSERT INTO `ci_erp_constants` VALUES ('144', '2', 'warning_type', 'Reassignment', 'Null', 'Null', '15-05-2021 04:34:13') ; 
INSERT INTO `ci_erp_constants` VALUES ('145', '2', 'warning_type', 'Non-discrimination', 'Null', 'Null', '15-05-2021 04:34:19') ; 
INSERT INTO `ci_erp_constants` VALUES ('146', '2', 'warning_type', 'Confidentiality', 'Null', 'Null', '15-05-2021 04:34:26') ; 
INSERT INTO `ci_erp_constants` VALUES ('147', '2', 'expense_type', 'Fuel Expense', 'Null', 'Null', '15-05-2021 06:04:48') ; 
INSERT INTO `ci_erp_constants` VALUES ('148', '2', 'expense_type', 'Advertising', 'Null', 'Null', '15-05-2021 06:05:11') ; 
INSERT INTO `ci_erp_constants` VALUES ('149', '2', 'expense_type', 'Salaries Expense', 'Null', 'Null', '15-05-2021 06:05:30') ; 
INSERT INTO `ci_erp_constants` VALUES ('150', '2', 'expense_type', 'Warranty Expense', 'Null', 'Null', '15-05-2021 06:05:58') ; 
INSERT INTO `ci_erp_constants` VALUES ('151', '2', 'expense_type', 'Other Expense', 'Null', 'Null', '15-05-2021 06:06:14') ; 
INSERT INTO `ci_erp_constants` VALUES ('152', '2', 'expense_type', 'Insurance', 'Null', 'Null', '15-05-2021 06:06:27') ; 
INSERT INTO `ci_erp_constants` VALUES ('153', '2', 'expense_type', 'Miscellaneous', 'Null', 'Null', '15-05-2021 06:06:51') ; 
INSERT INTO `ci_erp_constants` VALUES ('154', '2', 'expense_type', 'Payroll Tax', 'Null', 'Null', '15-05-2021 06:07:25') ; 
INSERT INTO `ci_erp_constants` VALUES ('155', '2', 'expense_type', 'Utilities', 'Null', 'Null', '15-05-2021 06:08:00') ; 
INSERT INTO `ci_erp_constants` VALUES ('156', '2', 'income_type', 'Capital Stock', 'Null', 'Null', '15-05-2021 06:09:03') ; 
INSERT INTO `ci_erp_constants` VALUES ('157', '2', 'income_type', 'Cash Over', 'Null', 'Null', '15-05-2021 06:09:15') ; 
INSERT INTO `ci_erp_constants` VALUES ('158', '2', 'income_type', 'Common Stock', 'Null', 'Null', '15-05-2021 06:09:28') ; 
INSERT INTO `ci_erp_constants` VALUES ('159', '2', 'income_type', 'Insurance Payable', 'Null', 'Null', '15-05-2021 06:11:42') ; 
INSERT INTO `ci_erp_constants` VALUES ('160', '2', 'income_type', 'Interest Income', 'Null', 'Null', '15-05-2021 06:11:53') ; 
INSERT INTO `ci_erp_constants` VALUES ('161', '2', 'expense_type', 'Interest Expense', 'Null', 'Null', '15-05-2021 06:12:12') ; 
INSERT INTO `ci_erp_constants` VALUES ('162', '2', 'income_type', 'Investment Income', 'Null', 'Null', '15-05-2021 06:12:55') ; 
INSERT INTO `ci_erp_constants` VALUES ('163', '2', 'income_type', 'Retained Earnings', 'Null', 'Null', '15-05-2021 06:13:39') ; 
INSERT INTO `ci_erp_constants` VALUES ('164', '2', 'income_type', 'Sales', 'Null', 'Null', '15-05-2021 06:14:27') ; 
INSERT INTO `ci_erp_constants` VALUES ('165', '2', 'income_type', 'Other Income', 'Null', 'Null', '15-05-2021 06:15:47') ; 
INSERT INTO `ci_erp_constants` VALUES ('166', '81', 'company_type', 'Private Limited Company', 'Null', 'Null', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_erp_constants` VALUES ('167', '2', 'company_type', 'Private Limited Company', 'Null', 'Null', '20-07-2021 05:23:07') ; 
INSERT INTO `ci_erp_constants` VALUES ('168', '2', 'religion', 'Humanism', 'Null', 'Null', '20-07-2021 05:23:42') ; 
INSERT INTO `ci_erp_constants` VALUES ('169', '2', 'assets_category', 'Desktop Computer', 'Null', 'Null', '21-09-2021 11:58:45') ; 
INSERT INTO `ci_erp_constants` VALUES ('170', '2', 'assets_category', 'IP Phone', 'Null', 'Null', '21-09-2021 11:58:56') ; 
INSERT INTO `ci_erp_constants` VALUES ('171', '2', 'assets_category', 'Web Cam', 'Null', 'Null', '21-09-2021 11:59:06') ; 
INSERT INTO `ci_erp_constants` VALUES ('172', '2', 'assets_category', 'Head Set', 'Null', 'Null', '21-09-2021 11:59:15') ; 
INSERT INTO `ci_erp_constants` VALUES ('173', '2', 'assets_category', 'Laptop', 'Null', 'Null', '21-09-2021 11:59:26') ; 
INSERT INTO `ci_erp_constants` VALUES ('174', '2', 'assets_brand', 'Dell', 'Null', 'Null', '21-09-2021 11:59:40') ; 
INSERT INTO `ci_erp_constants` VALUES ('175', '2', 'assets_brand', 'HP', 'Null', 'Null', '21-09-2021 11:59:47') ; 
INSERT INTO `ci_erp_constants` VALUES ('176', '2', 'assets_brand', 'Yealink', 'Null', 'Null', '21-09-2021 11:59:58') ; 
INSERT INTO `ci_erp_constants` VALUES ('177', '2', 'assets_brand', 'Logitech', 'Null', 'Null', '22-09-2021 12:00:12') ; 
INSERT INTO `ci_erp_constants` VALUES ('178', '2', 'exit_type', 'Resigned', 'Null', 'Null', '22-09-2021 07:28:22') ; 
INSERT INTO `ci_erp_constants` VALUES ('179', '2', 'exit_type', 'Terminated', 'Null', 'Null', '22-09-2021 07:28:33') ; 
INSERT INTO `ci_erp_constants` VALUES ('180', '2', 'expense_type', 'Office Rent', 'Null', 'Null', '22-09-2021 07:44:33') ;
#
# End of data contents of table ci_erp_constants
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------


#
# Delete any existing table `ci_erp_settings`
#

DROP TABLE IF EXISTS `ci_erp_settings`;


#
# Table structure of table `ci_erp_settings`
#

CREATE TABLE `ci_erp_settings` (
  `setting_id` int(111) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(255) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `trading_name` varchar(100) DEFAULT NULL,
  `registration_no` varchar(100) DEFAULT NULL,
  `government_tax` varchar(100) DEFAULT NULL,
  `company_type_id` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `country` int(11) NOT NULL DEFAULT '0',
  `address_1` text,
  `address_2` text,
  `city` varchar(200) DEFAULT NULL,
  `zipcode` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `default_currency` varchar(255) NOT NULL DEFAULT 'USD',
  `is_ssl_available` varchar(11) NOT NULL DEFAULT 'on',
  `currency_converter` mediumtext,
  `notification_position` varchar(255) NOT NULL,
  `notification_close_btn` varchar(255) NOT NULL,
  `notification_bar` varchar(255) NOT NULL,
  `date_format_xi` varchar(255) NOT NULL,
  `enable_email_notification` varchar(255) NOT NULL,
  `email_type` varchar(100) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `favicon` varchar(200) NOT NULL,
  `frontend_logo` varchar(200) NOT NULL,
  `other_logo` varchar(255) DEFAULT NULL,
  `animation_effect` varchar(255) NOT NULL,
  `animation_effect_modal` varchar(255) NOT NULL,
  `animation_effect_topmenu` varchar(255) NOT NULL,
  `default_language` varchar(200) NOT NULL DEFAULT 'en',
  `system_timezone` varchar(200) NOT NULL DEFAULT 'Asia/Bishkek',
  `paypal_email` varchar(100) NOT NULL,
  `paypal_sandbox` varchar(10) NOT NULL,
  `paypal_active` varchar(10) NOT NULL,
  `stripe_secret_key` varchar(200) NOT NULL,
  `stripe_publishable_key` varchar(200) NOT NULL,
  `stripe_active` varchar(10) NOT NULL,
  `online_payment_account` int(11) NOT NULL,
  `invoice_terms_condition` text,
  `auth_background` varchar(255) DEFAULT NULL,
  `hr_version` varchar(200) NOT NULL,
  `hr_release_date` varchar(100) NOT NULL,
  `updated_at` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_erp_settings (1 records)
#
 
INSERT INTO `ci_erp_settings` VALUES ('1', 'Employee Portal', 'AMR Kernel', 'AMR Kernel', 'AMR Kernel', 'AMR Kernel', '3', 'info@amrkernel.com', '+19084275008', '119', '9856 Mandani Road', 'Columbia YH POL', 'Missouri', '45896', 'Missouri', 'INR', '1', 'a:160:{s:3:\"USD\";s:1:\"1\";s:3:\"AED\";s:4:\"3.67\";s:3:\"AFN\";s:5:\"78.37\";s:3:\"ALL\";s:6:\"101.16\";s:3:\"AMD\";s:6:\"522.46\";s:3:\"ANG\";s:4:\"1.79\";s:3:\"AOA\";s:5:\"649.6\";s:3:\"ARS\";s:5:\"93.96\";s:3:\"AUD\";s:4:\"1.29\";s:3:\"AWG\";s:4:\"1.79\";s:3:\"AZN\";s:3:\"1.7\";s:3:\"BAM\";s:4:\"1.61\";s:3:\"BBD\";s:1:\"2\";s:3:\"BDT\";s:5:\"84.85\";s:3:\"BGN\";s:4:\"1.61\";s:3:\"BHD\";s:5:\"0.376\";s:3:\"BIF\";s:7:\"1957.01\";s:3:\"BMD\";s:1:\"1\";s:3:\"BND\";s:4:\"1.33\";s:3:\"BOB\";s:4:\"6.87\";s:3:\"BRL\";s:4:\"5.29\";s:3:\"BSD\";s:1:\"1\";s:3:\"BTN\";s:5:\"73.36\";s:3:\"BWP\";s:4:\"10.8\";s:3:\"BYN\";s:4:\"2.53\";s:3:\"BZD\";s:1:\"2\";s:3:\"CAD\";s:4:\"1.21\";s:3:\"CDF\";s:7:\"1988.03\";s:3:\"CHF\";s:5:\"0.903\";s:3:\"CLP\";s:5:\"707.8\";s:3:\"CNY\";s:4:\"6.44\";s:3:\"COP\";s:7:\"3711.94\";s:3:\"CRC\";s:6:\"612.38\";s:3:\"CUC\";s:1:\"1\";s:3:\"CUP\";s:5:\"25.75\";s:3:\"CVE\";s:5:\"90.86\";s:3:\"CZK\";s:5:\"21.02\";s:3:\"DJF\";s:6:\"177.72\";s:3:\"DKK\";s:4:\"6.15\";s:3:\"DOP\";s:5:\"57.01\";s:3:\"DZD\";s:6:\"133.47\";s:3:\"EGP\";s:5:\"15.64\";s:3:\"ERN\";s:2:\"15\";s:3:\"ETB\";s:5:\"42.71\";s:3:\"EUR\";s:5:\"0.824\";s:3:\"FJD\";s:4:\"2.04\";s:3:\"FKP\";s:4:\"0.71\";s:3:\"FOK\";s:4:\"6.15\";s:3:\"GBP\";s:4:\"0.71\";s:3:\"GEL\";s:4:\"3.42\";s:3:\"GGP\";s:4:\"0.71\";s:3:\"GHS\";s:4:\"5.76\";s:3:\"GIP\";s:4:\"0.71\";s:3:\"GMD\";s:5:\"51.95\";s:3:\"GNF\";s:7:\"9856.45\";s:3:\"GTQ\";s:4:\"7.69\";s:3:\"GYD\";s:6:\"213.16\";s:3:\"HKD\";s:4:\"7.77\";s:3:\"HNL\";s:2:\"24\";s:3:\"HRK\";s:4:\"6.21\";s:3:\"HTG\";s:5:\"87.66\";s:3:\"HUF\";s:6:\"293.53\";s:3:\"IDR\";s:8:\"14265.53\";s:3:\"ILS\";s:4:\"3.28\";s:3:\"IMP\";s:4:\"0.71\";s:3:\"INR\";s:5:\"73.36\";s:3:\"IQD\";s:7:\"1458.51\";s:3:\"IRR\";s:8:\"42064.89\";s:3:\"ISK\";s:6:\"124.36\";s:3:\"JMD\";s:6:\"150.88\";s:3:\"JOD\";s:5:\"0.709\";s:3:\"JPY\";s:6:\"109.38\";s:3:\"KES\";s:6:\"107.18\";s:3:\"KGS\";s:5:\"84.68\";s:3:\"KHR\";s:7:\"4066.72\";s:3:\"KID\";s:4:\"1.29\";s:3:\"KMF\";s:5:\"405.4\";s:3:\"KRW\";s:7:\"1128.24\";s:3:\"KWD\";s:3:\"0.3\";s:3:\"KYD\";s:5:\"0.833\";s:3:\"KZT\";s:6:\"427.47\";s:3:\"LAK\";s:7:\"9409.64\";s:3:\"LBP\";s:6:\"1507.5\";s:3:\"LKR\";s:6:\"196.68\";s:3:\"LRD\";s:6:\"171.74\";s:3:\"LSL\";s:4:\"14.1\";s:3:\"LYD\";s:4:\"4.46\";s:3:\"MAD\";s:4:\"8.85\";s:3:\"MDL\";s:5:\"17.75\";s:3:\"MGA\";s:7:\"3755.12\";s:3:\"MKD\";s:5:\"50.76\";s:3:\"MMK\";s:7:\"1558.16\";s:3:\"MNT\";s:7:\"2846.09\";s:3:\"MOP\";s:1:\"8\";s:3:\"MRU\";s:5:\"35.84\";s:3:\"MUR\";s:4:\"40.4\";s:3:\"MVR\";s:5:\"15.33\";s:3:\"MWK\";s:6:\"796.85\";s:3:\"MXN\";s:5:\"19.87\";s:3:\"MYR\";s:4:\"4.12\";s:3:\"MZN\";s:5:\"58.75\";s:3:\"NAD\";s:4:\"14.1\";s:3:\"NGN\";s:6:\"422.32\";s:3:\"NIO\";s:5:\"35.03\";s:3:\"NOK\";s:4:\"8.26\";s:3:\"NPR\";s:6:\"117.38\";s:3:\"NZD\";s:4:\"1.38\";s:3:\"OMR\";s:5:\"0.384\";s:3:\"PAB\";s:1:\"1\";s:3:\"PEN\";s:4:\"3.67\";s:3:\"PGK\";s:4:\"3.51\";s:3:\"PHP\";s:4:\"47.8\";s:3:\"PKR\";s:6:\"151.64\";s:3:\"PLN\";s:4:\"3.73\";s:3:\"PYG\";s:7:\"6556.73\";s:3:\"QAR\";s:4:\"3.64\";s:3:\"RON\";s:4:\"4.06\";s:3:\"RSD\";s:5:\"97.23\";s:3:\"RUB\";s:5:\"73.95\";s:3:\"RWF\";s:6:\"999.71\";s:3:\"SAR\";s:4:\"3.75\";s:3:\"SBD\";s:4:\"7.86\";s:3:\"SCR\";s:5:\"15.44\";s:3:\"SDG\";s:6:\"398.45\";s:3:\"SEK\";s:4:\"8.36\";s:3:\"SGD\";s:4:\"1.33\";s:3:\"SHP\";s:4:\"0.71\";s:3:\"SLL\";s:8:\"10261.51\";s:3:\"SOS\";s:6:\"578.45\";s:3:\"SRD\";s:5:\"14.15\";s:3:\"SSP\";s:6:\"177.64\";s:3:\"STN\";s:5:\"20.19\";s:3:\"SYP\";s:7:\"1286.59\";s:3:\"SZL\";s:4:\"14.1\";s:3:\"THB\";s:5:\"31.37\";s:3:\"TJS\";s:5:\"11.31\";s:3:\"TMT\";s:3:\"3.5\";s:3:\"TND\";s:4:\"2.72\";s:3:\"TOP\";s:4:\"2.23\";s:3:\"TRY\";s:4:\"8.46\";s:3:\"TTD\";s:3:\"6.8\";s:3:\"TVD\";s:4:\"1.29\";s:3:\"TWD\";s:5:\"27.93\";s:3:\"TZS\";s:7:\"2314.62\";s:3:\"UAH\";s:5:\"27.62\";s:3:\"UGX\";s:7:\"3529.82\";s:3:\"UYU\";s:5:\"44.13\";s:3:\"UZS\";s:8:\"10454.23\";s:3:\"VES\";s:10:\"2959224.82\";s:3:\"VND\";s:8:\"23123.31\";s:3:\"VUV\";s:6:\"107.04\";s:3:\"WST\";s:4:\"2.51\";s:3:\"XAF\";s:6:\"540.53\";s:3:\"XCD\";s:3:\"2.7\";s:3:\"XDR\";s:5:\"0.695\";s:3:\"XOF\";s:6:\"540.53\";s:3:\"XPF\";s:5:\"98.33\";s:3:\"YER\";s:6:\"249.75\";s:3:\"ZAR\";s:4:\"14.1\";s:3:\"ZMW\";s:5:\"22.42\";}', 'toast-top-center', '0', 'true', 'Y-m-d', '1', 'phpmail', 'new-logo.png', 'profile pic.jpg', 'logo.png', 'new-logo1.png', 'fadeInDown', 'tada', 'tada', 'en', 'Asia/Kolkata', 'your.paypal.email@domain.com', 'yes', 'no', 'sk_test_51IrqtWJck1huBCXGjafHHFZzzK28jEJq7dsRRzegUH9uQ5X7nYMhPwcSueFUlixy1M86E4o3LhZU1jsSYgCAclNW00BXVEUbwX', 'pk_test_51IrqtWJck1huBCXGe7b6UalwItsUyRcMaqRgiWINpavr859Rcd3XYLnlm3pI9rVV3cVkaLKSIQMgTxupa6774r3b00PkCajPhD', 'no', '2', 'lorem ipsum dolor sit', '4859202', '1.0.0', '2021-05-09', '09-05-2021 06:36:23') ;
#
# End of data contents of table ci_erp_settings
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------


#
# Delete any existing table `ci_erp_users`
#

DROP TABLE IF EXISTS `ci_erp_users`;


#
# Table structure of table `ci_erp_users`
#

CREATE TABLE `ci_erp_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) DEFAULT NULL,
  `user_type` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `trading_name` varchar(100) DEFAULT NULL,
  `registration_no` varchar(100) DEFAULT NULL,
  `government_tax` varchar(100) DEFAULT NULL,
  `company_type_id` int(11) DEFAULT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `gender` varchar(20) NOT NULL,
  `address_1` text,
  `address_2` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `last_login_date` varchar(255) DEFAULT NULL,
  `last_logout_date` varchar(200) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  `is_logged_in` int(11) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_erp_users (30 records)
#
 
INSERT INTO `ci_erp_users` VALUES ('2', '0', 'company', '1', '0', 'Aamir', 'Raza', 'info@amrkernel.com', 'manager', '$2y$12$.4Im86HwfpDh1exM31KXEOJh0HrNqOy.sYRS1anN3YxB8ulNWkFp.', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', 'TRD-9853142', 'RG-153974520', 'TX-74521583', '166', 'Aamir Raza EMP A027.jfif', '07065433076', '1', 'G-13 Sector-6 Noida Gautam Buddha,', 'Uttar Pradesh', 'Noida', 'Uttar Pradesh', '201301', '99', '22-09-2021 14:39:58', '22-09-2021 19:45:49', '103.88.59.67', '1', '15-05-2021 08:11:26') ; 
INSERT INTO `ci_erp_users` VALUES ('3', '1', 'staff', '0', '2', 'Vikram', 'Ranganathappa', 'vikram@ramassociates.us', 'vikram@ramassociates.us', '$2y$12$X6UyXz2.lf.L1KTpQR/BPuzzDBdniFAwACXsemWV1w5Qdr66On6T2', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Vikram R EMP A010.jfif', '1234567890', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 03:26:42') ; 
INSERT INTO `ci_erp_users` VALUES ('4', '1', 'staff', '0', '2', 'Dinesh', 'HR', 'dinesh.r@ramassociates.us', 'dinesh.r@ramassociates.us', '$2y$12$cwNknAQJqJX1A2UnVJ3zp.FIR4w1ETNpTGl4ZxdE/I3J1NkSktc6e', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Dinesh H R EMP A015.jfif', '123456', '1', '', '', '', '', '', '0', '01-09-2021 07:59:03', '09-09-2021 15:36:38', '122.172.215.26', '0', '21-07-2021 04:01:42') ; 
INSERT INTO `ci_erp_users` VALUES ('5', '1', 'staff', '1', '2', 'Suresh', 'KS', 'suresh@ramassociates.us', 'suresh@ramassociates.us', '$2y$12$LQxkhmxULzcBdqy63blijORIkh.iOxr9uGJ2bjn8MdDd7I6ylThmC', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Suresh K S EMP A014.jfif', '123456', '1', '', '', '', '', '', '0', '16-09-2021 12:56:47', '11-09-2021 00:54:41', '122.172.21.76', '1', '21-07-2021 04:21:35') ; 
INSERT INTO `ci_erp_users` VALUES ('6', '1', 'staff', '0', '2', 'Santosh', 'Kumar TS', 'santosh.ts@ramassociates.us', 'santosh.ts@ramassociates.us', '$2y$12$wXzVuQSoMOoyy/eh9.SpkOVOsyxIPmc9grx.hcT3e7lmnW4Wa93yG', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Santhosh T S EMP A011.jfif', '123456', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 04:23:27') ; 
INSERT INTO `ci_erp_users` VALUES ('7', '1', 'staff', '0', '2', 'Pushpa', 'Latha R', 'pushpalatha.r@ramassociates.us', 'pushpalatha.r@ramassociates.us', '$2y$12$bfFxfnynhrikeyKa6DQ/YeUxqCxfVSXhnfJmZmdxyzdZSrJpKI1Oq', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Pushpalatha R EMP A023.jfif', '12345', '2', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 04:25:55') ; 
INSERT INTO `ci_erp_users` VALUES ('8', '1', 'staff', '0', '2', 'Venkat', 'Swamy R', 'venkat.s@ramassociates.us', 'venkat.s@ramassociates.us', '$2y$12$aSKjdOGUUFZhkiJ8zrxUPePr0icc/SHy4jhZ5jNlExsEay4xXfRSi', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Venkat S EMP A018.jfif', '12345', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 04:28:03') ; 
INSERT INTO `ci_erp_users` VALUES ('9', '1', 'staff', '0', '2', 'Vinay', 'Chandrashekariah', 'vinay.c@ramassociates.us', 'vinay.c@ramassociates.us', '$2y$12$Z.GxCpLf6kE6qSyiLWoBjudtr.2l/F.PzhJJOna46WaI2O8bEoVRW', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Vinay C EMP A017.jfif', '1234', '1', '', '', '', '', '', '0', '02-09-2021 06:56:57', '09-09-2021 15:36:38', '117.96.243.130', '0', '21-07-2021 04:30:31') ; 
INSERT INTO `ci_erp_users` VALUES ('10', '1', 'staff', '0', '2', 'Srilatha', 'HG', 'srilatha@ramassociates.us', 'srilatha@ramassociates.us', '$2y$12$IaNklSEWmzBJO8cqkE109.pW.Vy2GnUec8VHU/a/ZFTxLCYGa2MS6', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Srilatha H G EMP A031.jfif', '1234', '2', '', '', '', '', '', '0', '30-07-2021 09:16:49', '09-09-2021 15:36:38', '122.171.125.0', '0', '21-07-2021 04:32:11') ; 
INSERT INTO `ci_erp_users` VALUES ('11', '1', 'staff', '0', '2', 'Rashmi', 'Narasimhaiah', 'rashmi@ramassociates.us', 'rashmi@ramassociates.us', '$2y$12$T5blHjdbAI9CcoTCovo0gu1pW2qlGle/ZrD.t/fynexevAJ8TNtJC', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Rashmi N EMP A030.jfif', '1234', '2', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 04:33:51') ; 
INSERT INTO `ci_erp_users` VALUES ('12', '1', 'staff', '0', '2', 'Chaitanya', 'Rangappa', 'chaithanya@ramassociates.us', 'chaithanya@ramassociates.us', '$2y$12$6dhgX32RmHDXcFg78Psv8e73vsVR2Ec5r.TmbxhV9c0fRXolFJJM6', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Chaithanya EMP A022.jfif', '1234', '2', '', '', '', '', '', '0', '20-07-2021 21:05:09', '09-09-2021 15:36:38', '157.35.84.164', '0', '21-07-2021 04:45:07') ; 
INSERT INTO `ci_erp_users` VALUES ('13', '1', 'staff', '0', '2', 'Sabeeha', 'Khanum', 'sabeeha@ramassociates.us', 'sabeeha@ramassociates.us', '$2y$12$YeoZwjod6u56kfcen3prgOQ8TpIBCMwLgyVZYfZ2wYNJqNiEXYJ1i', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Sabeeha Khanum EMP A025.jfif', '1234', '2', '', '', '', '', '', '0', '02-09-2021 02:46:35', '09-09-2021 15:36:38', '106.193.36.152', '0', '21-07-2021 04:46:50') ; 
INSERT INTO `ci_erp_users` VALUES ('14', '1', 'staff', '0', '2', 'Latha', 'Yellappa', 'latha@ramassociates.us', 'latha@ramassociates.us', '$2y$12$uDBXDfSlDjDNmUaOONL0euQx/xXOEa44XttGdDAJ7qgt3cPkHsFqK', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Latha Y EMP A034 .jfif', '1234', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 05:06:09') ; 
INSERT INTO `ci_erp_users` VALUES ('15', '1', 'staff', '0', '2', 'Mahalakshmi', 'BG', 'mahalakshmi@ramassociates.us', 'mahalakshmi@ramassociates.us', '$2y$12$oGZV2yyNKgzbHy8ctLy57uo6Q2iIt99ChL0OwPPWWk7sNhFOokju2', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Mahalakshmi B G EMP A032.jfif', '1234', '2', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 05:07:56') ; 
INSERT INTO `ci_erp_users` VALUES ('16', '1', 'staff', '0', '2', 'Chaithra', 'E', 'chaithra@ramassociates.us', 'chaithra@ramassociates.us', '$2y$12$7lhiyTBqoIJmBHMX0pH4gOZClmUIyXXBo8ogUjQHnE0S39bis5q7G', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Chaithra E EMP A021.jfif', '1234', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 05:09:28') ; 
INSERT INTO `ci_erp_users` VALUES ('17', '1', 'staff', '0', '2', 'Hitesh', 'Kumar', 'hiteshk@ramassociates.us', 'hiteshk@ramassociates.us', '$2y$12$0rT1jGyaaPQ9CI7F/4zTXurPmYlgGVINBP917mjMhBfkNBcyMWzn2', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Hithesh Kumar EMP A020.jfif', '1234', '1', '', '', '', '', '', '0', '20-07-2021 21:01:07', '09-09-2021 15:36:38', '157.35.84.164', '0', '21-07-2021 05:10:53') ; 
INSERT INTO `ci_erp_users` VALUES ('18', '1', 'staff', '0', '2', 'Vandana', 'KJ', 'vandana.kj@ramassociates.us', 'vandana.kj@ramassociates.us', '$2y$12$u3tbwxI68dnkyYuOq4g/nOqJvHF/02RH0rnr7TGpALMrKaOpoA0Ti', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Vandana K J EMP A024.jfif', '1234', '2', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 05:12:23') ; 
INSERT INTO `ci_erp_users` VALUES ('19', '1', 'staff', '0', '2', 'Shaziya', 'Banu', 'shaziya.b@ramassociates.us', 'shaziya.b@ramassociates.us', '$2y$12$OOkvh6PxQ5EhcIotPjHbr..D4cEZEMTvZBuVIqOj6eIoMFK13dswq', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'Shazia Banu EMP A028.jfif', '1234', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '21-07-2021 05:14:15') ; 
INSERT INTO `ci_erp_users` VALUES ('20', '1', 'staff', '0', '2', 'Manu', 'N', 'manu.n@ramassociates.us', 'manu.n@ramassociates.us', '$2y$12$V3eDzTVQShOKnoQ/gDpJ9uLQ3fEsCLpf2ONUME5c8/oteXWtInwlK', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '1234', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '07-09-2021 04:50:08') ; 
INSERT INTO `ci_erp_users` VALUES ('21', '1', 'staff', '0', '2', 'Sadiq', 'M', 'sadiq.m@ramassociates.us', 'sadiq.m@ramassociates.us', '$2y$12$G/jfoLTXAh52Y6t.dVu0kuIFBfhcxAurxvsAzRZn.HaA6qTk9OGzK', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '1234', '1', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '07-09-2021 04:55:15') ; 
INSERT INTO `ci_erp_users` VALUES ('22', '1', 'staff', '0', '2', 'Shruthi', 'MR', 'shruthi.m@ramassociates.us', 'shruthi.m@ramassociates.us', '$2y$12$63RVP.20Xg4VQcfnoyxqE.834vaC5L9Kj0wKLxEY/nl.HW3vNJm3i', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '12345', '2', '', '', '', '', '', '0', '0', '09-09-2021 15:36:38', '0', '0', '07-09-2021 05:02:01') ; 
INSERT INTO `ci_erp_users` VALUES ('23', '1', 'staff', '1', '2', 'Sunny', 'Kumar', 'sunny.kumar@amrkernel.com', 'sunny.kumar@amrkernel.com', '$2y$12$AnhDmP1Yyf0UTQnLxceMcOwscM.NVnlCkmK5brlvygygbgk.5sbki', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '987654321', '1', '', '', '', '', '', '0', '17-09-2021 10:04:11', '17-09-2021 20:36:32', '103.88.59.67', '0', '07-09-2021 05:32:48') ; 
INSERT INTO `ci_erp_users` VALUES ('24', '1', 'staff', '1', '2', 'Naveen', 'Chandra', 'naveen.chandra@amrkernel.com', 'naveen.chandra@amrkernel.com', '$2y$12$4y8znpElOxBTJrWaeSZd/ORwt2kQYSh.U8awEUd4.A7TZqLrxM7iG', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '654321987', '1', '', '', '', '', '', '0', '22-09-2021 09:16:13', '22-09-2021 19:17:44', '103.88.59.67', '1', '07-09-2021 05:34:39') ; 
INSERT INTO `ci_erp_users` VALUES ('25', '1', 'staff', '1', '2', 'Faizan', 'Sarwar', 'faizan@amrkernel.com', 'faizan@amrkernel.com', '$2y$12$AraNqwaLYwVmXkbTcg31Q.AXWGIUzwMh.PmcAIZP.sWeMCm.wmmEi', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '321456789', '1', '', '', '', '', '', '0', '22-09-2021 08:50:16', '22-09-2021 02:22:47', '103.88.59.67', '1', '07-09-2021 05:36:42') ; 
INSERT INTO `ci_erp_users` VALUES ('26', '1', 'staff', '0', '2', 'Nethravathi', 'Manjunath', 'nethra.manjunath@RamAssociates.us', 'Nethravathi', '$2y$12$3PgRB8h1Rq9qiDBH/o/6cOQ5gqlx7f2lkILjgD..ZJfOP1zt.aqMm', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'macbook.jpg', '1234', '1', '', '', '', '', '', '0', '0', '0', '0', '0', '21-09-2021 10:01:42') ; 
INSERT INTO `ci_erp_users` VALUES ('27', '1', 'staff', '0', '2', 'Sharada', 'Manjunath', 'sharada.manjunath@RamAssociates.us', 'Sharada', '$2y$12$j4QQb4h/6UA3ArLwmIroU.HdhhpNN87PG8Clu76Vml.yQoIzH92Om', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'macbook.jpg', '1234', '2', '', '', '', '', '', '0', '0', '0', '0', '0', '21-09-2021 10:04:25') ; 
INSERT INTO `ci_erp_users` VALUES ('28', '1', 'staff', '1', '2', 'Vaishnavi', 'Gowda', 'vaishnavi.gowda@RamAssociates.us', 'Vaishnavi Gowda', '$2y$12$zun9vLnigitKU4cktCHeD.pIaowpdnU.F0LCMoClxoHeKxn9M66TC', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'macbook.jpg', '1234', '2', '', '', '', '', '', '0', '0', '0', '0', '0', '21-09-2021 10:07:32') ; 
INSERT INTO `ci_erp_users` VALUES ('29', '1', 'staff', '0', '2', 'Asha', 'Gowda', 'asha.gowda@Ramassociates.us', 'asha.gowda@Ramassociates.us', '$2y$12$w/Q0O0p/s/IXNS2UStMu5Omoh7saTkVLuVSGwFhCd9agr5H7W0H4W', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '12345', '2', '', '', '', '', '', '0', '0', '0', '0', '0', '21-09-2021 11:41:39') ; 
INSERT INTO `ci_erp_users` VALUES ('30', '1', 'staff', '0', '2', 'Chaithra', 'Hemanth', 'chaithra.hemanth@ramassociates.us', 'chaithra.hemanth@ramassociates.us', '$2y$12$5tEU9ZM0vopebZJLnNAAWOWfYmYtjeuaNYHaOYblhpECyla2x/mMG', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '123456', '1', '', '', '', '', '', '0', '0', '0', '0', '0', '21-09-2021 11:44:12') ; 
INSERT INTO `ci_erp_users` VALUES ('31', '1', 'staff', '0', '2', 'Smrutha ', 'Devprasad', 'smrutha.devprasad@ramassociates.us', 'smrutha.devprasad@ramassociates.us', '$2y$12$XaypA4yx2.hbJtjweURAL.4vy6.w32wa8y18iRdNLAZywjW4WtOee', 'ABYZ IT AND CALL SERVICES PRIVATE LIMITED', '', '', '', '0', 'user-placeholder.jpg', '1234', '2', '', '', '', '', '', '0', '0', '0', '0', '0', '21-09-2021 11:55:58') ;
#
# End of data contents of table ci_erp_users
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------


#
# Delete any existing table `ci_erp_users_details`
#

DROP TABLE IF EXISTS `ci_erp_users_details`;


#
# Table structure of table `ci_erp_users_details`
#

CREATE TABLE `ci_erp_users_details` (
  `staff_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(255) NOT NULL,
  `pf_number` bigint(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `office_shift_id` int(11) NOT NULL,
  `current_ctc` decimal(65,2) NOT NULL,
  `basic_salary` decimal(65,2) NOT NULL,
  `hourly_rate` decimal(65,2) NOT NULL,
  `salay_type` int(11) NOT NULL,
  `role_description` mediumtext,
  `date_of_joining` varchar(200) DEFAULT NULL,
  `date_of_leaving` varchar(200) DEFAULT NULL,
  `date_of_birth` varchar(200) DEFAULT NULL,
  `marital_status` int(11) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `blood_group` varchar(200) DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `bio` mediumtext,
  `experience` int(11) DEFAULT NULL,
  `fb_profile` mediumtext,
  `twitter_profile` mediumtext,
  `gplus_profile` mediumtext,
  `linkedin_profile` mediumtext,
  `account_title` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `swift_code` varchar(255) DEFAULT NULL,
  `bank_branch` mediumtext,
  `contact_full_name` varchar(200) DEFAULT NULL,
  `contact_phone_no` varchar(200) DEFAULT NULL,
  `contact_email` varchar(200) DEFAULT NULL,
  `contact_address` mediumtext,
  `created_at` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`staff_details_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_erp_users_details (29 records)
#
 
INSERT INTO `ci_erp_users_details` VALUES ('1', '3', 'AMRSTAFF-KA-29-2020', '101694902573', '1', '2', '2', '74000.00', '37000.00', '0.00', '1', 'Enter role description here..', '2020-03-08', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 03:26:42') ; 
INSERT INTO `ci_erp_users_details` VALUES ('2', '4', 'AMRSTAFF-KA-26-2020', '101694884912', '9', '2', '2', '50000.00', '25000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:01:42') ; 
INSERT INTO `ci_erp_users_details` VALUES ('3', '5', 'AMRSTAFF-KA-28-2020', '101694890966', '1', '2', '2', '54000.00', '27000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:21:35') ; 
INSERT INTO `ci_erp_users_details` VALUES ('4', '6', 'AMRSTAFF-KA-27-2020', '101696251829', '1', '2', '2', '52000.00', '26000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:23:27') ; 
INSERT INTO `ci_erp_users_details` VALUES ('5', '7', 'AMRSTAFF-KA-13-2020', '101694631346', '1', '1', '1', '17500.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:25:55') ; 
INSERT INTO `ci_erp_users_details` VALUES ('6', '8', 'AMRSTAFF-KA-24-2020', '101694896010', '1', '1', '2', '20000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:28:03') ; 
INSERT INTO `ci_erp_users_details` VALUES ('7', '9', 'AMRSTAFF-KA-25-2020', '101605266102', '1', '1', '2', '20000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:30:31') ; 
INSERT INTO `ci_erp_users_details` VALUES ('8', '10', 'AMRSTAFF-KA-19-2020', '101694804732', '1', '1', '2', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:32:11') ; 
INSERT INTO `ci_erp_users_details` VALUES ('9', '11', 'AMRSTAFF-KA-15-2020', '101694644288', '1', '1', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:33:51') ; 
INSERT INTO `ci_erp_users_details` VALUES ('10', '12', 'AMRSTAFF-KA-08-2020', '101694577914', '9', '1', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:45:07') ; 
INSERT INTO `ci_erp_users_details` VALUES ('11', '13', 'AMRSTAFF-KA-16-2020', '101174594457', '10', '8', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 04:46:50') ; 
INSERT INTO `ci_erp_users_details` VALUES ('12', '14', 'AMRSTAFF-KA-11-2020', '101694619158', '1', '1', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 05:06:09') ; 
INSERT INTO `ci_erp_users_details` VALUES ('13', '15', 'AMRSTAFF-KA-12-2020', '101694626975', '1', '1', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 05:07:56') ; 
INSERT INTO `ci_erp_users_details` VALUES ('14', '16', 'AMRSTAFF-KA-09-2020', '101694590357', '9', '1', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 05:09:28') ; 
INSERT INTO `ci_erp_users_details` VALUES ('15', '17', 'AMRSTAFF-KA-23-2020', '101009558599', '1', '1', '2', '20000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 05:10:53') ; 
INSERT INTO `ci_erp_users_details` VALUES ('16', '18', 'AMRSTAFF-KA-20-2020', '101696200518', '1', '1', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 05:12:23') ; 
INSERT INTO `ci_erp_users_details` VALUES ('17', '19', 'AMRSTAFF-KA-17-2020', '0', '1', '1', '1', '15000.00', '15000.00', '0.00', '1', 'Enter role description here..', '21-07-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-07-2021 05:14:15') ; 
INSERT INTO `ci_erp_users_details` VALUES ('18', '20', '185136', '0', '1', '1', '2', '0.00', '15000.00', '0.00', '1', 'Enter role description here..', '07-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '07-09-2021 04:50:08') ; 
INSERT INTO `ci_erp_users_details` VALUES ('19', '21', '391535', '0', '1', '1', '2', '0.00', '18000.00', '0.00', '1', 'Enter role description here..', '07-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '07-09-2021 04:55:15') ; 
INSERT INTO `ci_erp_users_details` VALUES ('20', '22', '391535', '0', '1', '1', '1', '0.00', '15000.00', '0.00', '1', 'Enter role description here..', '07-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '07-09-2021 05:02:01') ; 
INSERT INTO `ci_erp_users_details` VALUES ('21', '23', '174522', '0', '8', '5', '2', '0.00', '20000.00', '0.00', '1', 'Enter role description here..', '07-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '07-09-2021 05:32:48') ; 
INSERT INTO `ci_erp_users_details` VALUES ('22', '24', '174522', '0', '8', '11', '2', '30000.00', '30000.00', '0.00', '1', 'Enter role description here..', '07-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '07-09-2021 05:34:39') ; 
INSERT INTO `ci_erp_users_details` VALUES ('23', '25', '174522', '0', '8', '7', '2', '0.00', '25000.00', '0.00', '1', 'Enter role description here..', '07-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '07-09-2021 05:36:42') ; 
INSERT INTO `ci_erp_users_details` VALUES ('24', '26', '849887', '0', '11', '9', '1', '0.00', '0.00', '0.00', '1', 'Enter role description here..', '21-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-09-2021 10:01:42') ; 
INSERT INTO `ci_erp_users_details` VALUES ('25', '27', '066899', '0', '11', '9', '1', '0.00', '0.00', '0.00', '1', 'Enter role description here..', '21-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-09-2021 10:04:25') ; 
INSERT INTO `ci_erp_users_details` VALUES ('26', '28', '216175', '0', '11', '9', '1', '0.00', '0.00', '0.00', '1', 'Enter role description here..', '21-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-09-2021 10:07:32') ; 
INSERT INTO `ci_erp_users_details` VALUES ('27', '29', '910968', '0', '11', '9', '1', '0.00', '0.00', '0.00', '1', 'Enter role description here..', '21-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-09-2021 11:41:39') ; 
INSERT INTO `ci_erp_users_details` VALUES ('28', '30', '910968', '0', '9', '1', '1', '0.00', '0.00', '0.00', '1', 'Enter role description here..', '21-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-09-2021 11:44:12') ; 
INSERT INTO `ci_erp_users_details` VALUES ('29', '31', '512687', '0', '9', '10', '2', '0.00', '0.00', '0.00', '1', 'Enter role description here..', '21-09-2021', '', '', '0', '0', '', '0', 'Enter staff bio here..', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21-09-2021 11:55:58') ;
#
# End of data contents of table ci_erp_users_details
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------


#
# Delete any existing table `ci_erp_users_role`
#

DROP TABLE IF EXISTS `ci_erp_users_role`;


#
# Table structure of table `ci_erp_users_role`
#

CREATE TABLE `ci_erp_users_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) DEFAULT NULL,
  `role_access` varchar(200) DEFAULT NULL,
  `role_resources` text,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_erp_users_role (0 records)
#

#
# End of data contents of table ci_erp_users_role
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------


#
# Delete any existing table `ci_estimates`
#

DROP TABLE IF EXISTS `ci_estimates`;


#
# Table structure of table `ci_estimates`
#

CREATE TABLE `ci_estimates` (
  `estimate_id` int(111) NOT NULL AUTO_INCREMENT,
  `estimate_number` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(111) NOT NULL,
  `estimate_month` varchar(255) DEFAULT NULL,
  `estimate_date` varchar(255) NOT NULL,
  `estimate_due_date` varchar(255) NOT NULL,
  `sub_total_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `discount_type` varchar(11) NOT NULL,
  `discount_figure` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_tax` decimal(65,2) NOT NULL DEFAULT '0.00',
  `tax_type` varchar(100) DEFAULT NULL,
  `total_discount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `grand_total` decimal(65,2) NOT NULL DEFAULT '0.00',
  `estimate_note` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`estimate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_estimates (0 records)
#

#
# End of data contents of table ci_estimates
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------


#
# Delete any existing table `ci_estimates_items`
#

DROP TABLE IF EXISTS `ci_estimates_items`;


#
# Table structure of table `ci_estimates_items`
#

CREATE TABLE `ci_estimates_items` (
  `estimate_item_id` int(111) NOT NULL AUTO_INCREMENT,
  `estimate_id` int(111) NOT NULL,
  `project_id` int(111) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_qty` varchar(255) NOT NULL,
  `item_unit_price` decimal(65,2) NOT NULL DEFAULT '0.00',
  `item_sub_total` decimal(65,2) NOT NULL DEFAULT '0.00',
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`estimate_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_estimates_items (0 records)
#

#
# End of data contents of table ci_estimates_items
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------


#
# Delete any existing table `ci_events`
#

DROP TABLE IF EXISTS `ci_events`;


#
# Table structure of table `ci_events`
#

CREATE TABLE `ci_events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` varchar(255) DEFAULT NULL,
  `event_title` varchar(255) NOT NULL,
  `event_date` varchar(255) NOT NULL,
  `event_time` varchar(255) NOT NULL,
  `event_note` mediumtext NOT NULL,
  `event_color` varchar(200) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_events (0 records)
#

#
# End of data contents of table ci_events
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------


#
# Delete any existing table `ci_finance_accounts`
#

DROP TABLE IF EXISTS `ci_finance_accounts`;


#
# Table structure of table `ci_finance_accounts`
#

CREATE TABLE `ci_finance_accounts` (
  `account_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_balance` decimal(65,2) NOT NULL DEFAULT '0.00',
  `account_opening_balance` decimal(65,2) NOT NULL DEFAULT '0.00',
  `account_number` varchar(255) NOT NULL,
  `branch_code` varchar(255) NOT NULL,
  `bank_branch` text NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_finance_accounts (0 records)
#

#
# End of data contents of table ci_finance_accounts
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------


#
# Delete any existing table `ci_finance_entity`
#

DROP TABLE IF EXISTS `ci_finance_entity`;


#
# Table structure of table `ci_finance_entity`
#

CREATE TABLE `ci_finance_entity` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_finance_entity (0 records)
#

#
# End of data contents of table ci_finance_entity
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------


#
# Delete any existing table `ci_finance_membership_invoices`
#

DROP TABLE IF EXISTS `ci_finance_membership_invoices`;


#
# Table structure of table `ci_finance_membership_invoices`
#

CREATE TABLE `ci_finance_membership_invoices` (
  `membership_invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(50) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `membership_id` int(11) NOT NULL,
  `subscription_id` varchar(50) DEFAULT NULL,
  `membership_type` varchar(200) NOT NULL,
  `subscription` varchar(200) NOT NULL,
  `invoice_month` varchar(255) DEFAULT NULL,
  `membership_price` decimal(65,2) NOT NULL DEFAULT '0.00',
  `payment_method` varchar(200) NOT NULL,
  `transaction_date` varchar(200) NOT NULL,
  `description` mediumtext NOT NULL,
  `receipt_url` longtext,
  `source_info` varchar(10) DEFAULT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`membership_invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_finance_membership_invoices (1 records)
#
 
INSERT INTO `ci_finance_membership_invoices` VALUES ('4', 'txn_1IrrDtJck1huBCXGA8vOl02B', '2', '6', '100585963', 'Pro Plan', '2', '2021-05', '59.00', 'Stripe', '2021-05-16 05:10:07', 'Free server unlimited approx 255k+ Premium collection', 'stripe.com', 'visa', '2021-05-16 05:10:07') ;
#
# End of data contents of table ci_finance_membership_invoices
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------


#
# Delete any existing table `ci_finance_transactions`
#

DROP TABLE IF EXISTS `ci_finance_transactions`;


#
# Table structure of table `ci_finance_transactions`
#

CREATE TABLE `ci_finance_transactions` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `transaction_date` varchar(255) NOT NULL,
  `transaction_type` varchar(100) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `entity_type` varchar(100) DEFAULT NULL,
  `entity_category_id` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `dr_cr` enum('dr','cr') NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `attachment_file` varchar(100) DEFAULT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_finance_transactions (0 records)
#

#
# End of data contents of table ci_finance_transactions
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------


#
# Delete any existing table `ci_holidays`
#

DROP TABLE IF EXISTS `ci_holidays`;


#
# Table structure of table `ci_holidays`
#

CREATE TABLE `ci_holidays` (
  `holiday_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `event_name` varchar(200) NOT NULL,
  `description` mediumtext NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `is_publish` tinyint(1) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`holiday_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_holidays (0 records)
#

#
# End of data contents of table ci_holidays
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------


#
# Delete any existing table `ci_invoices`
#

DROP TABLE IF EXISTS `ci_invoices`;


#
# Table structure of table `ci_invoices`
#

CREATE TABLE `ci_invoices` (
  `invoice_id` int(111) NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_id` int(111) NOT NULL,
  `invoice_month` varchar(255) DEFAULT NULL,
  `invoice_date` varchar(255) NOT NULL,
  `invoice_due_date` varchar(255) NOT NULL,
  `sub_total_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `discount_type` varchar(11) NOT NULL,
  `discount_figure` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_tax` decimal(65,2) NOT NULL DEFAULT '0.00',
  `tax_type` varchar(100) DEFAULT NULL,
  `total_discount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `grand_total` decimal(65,2) NOT NULL DEFAULT '0.00',
  `invoice_note` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_invoices (0 records)
#

#
# End of data contents of table ci_invoices
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------


#
# Delete any existing table `ci_invoices_items`
#

DROP TABLE IF EXISTS `ci_invoices_items`;


#
# Table structure of table `ci_invoices_items`
#

CREATE TABLE `ci_invoices_items` (
  `invoice_item_id` int(111) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(111) NOT NULL,
  `project_id` int(111) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_qty` varchar(255) NOT NULL,
  `item_unit_price` decimal(65,2) NOT NULL DEFAULT '0.00',
  `item_sub_total` decimal(65,2) NOT NULL DEFAULT '0.00',
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`invoice_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_invoices_items (0 records)
#

#
# End of data contents of table ci_invoices_items
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------


#
# Delete any existing table `ci_languages`
#

DROP TABLE IF EXISTS `ci_languages`;


#
# Table structure of table `ci_languages`
#

CREATE TABLE `ci_languages` (
  `language_id` int(111) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(255) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `language_flag` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_languages (10 records)
#
 
INSERT INTO `ci_languages` VALUES ('1', 'English', 'en', 'en.gif', '1', '09-05-2021 06:36:23') ; 
INSERT INTO `ci_languages` VALUES ('3', 'Russian', 'ru', 'ru.gif', '0', '14-05-2021 08:22:21') ; 
INSERT INTO `ci_languages` VALUES ('4', 'Dutch', 'nl', 'nl.gif', '0', '14-05-2021 09:39:11') ; 
INSERT INTO `ci_languages` VALUES ('5', 'Portuguese', 'br', 'br.gif', '0', '15-05-2021 12:28:41') ; 
INSERT INTO `ci_languages` VALUES ('6', 'Vietnamese', 'vn', 'vn.gif', '0', '15-05-2021 12:29:04') ; 
INSERT INTO `ci_languages` VALUES ('7', 'Spanish', 'es', 'es.gif', '0', '15-05-2021 12:30:13') ; 
INSERT INTO `ci_languages` VALUES ('8', 'Italiano', 'it', 'it.gif', '0', '15-05-2021 12:30:54') ; 
INSERT INTO `ci_languages` VALUES ('9', 'Turkish', 'tr', 'tr.gif', '0', '15-05-2021 12:31:21') ; 
INSERT INTO `ci_languages` VALUES ('10', 'French', 'fr', 'fr.gif', '0', '15-05-2021 12:31:39') ; 
INSERT INTO `ci_languages` VALUES ('11', 'Chinese', 'cn', 'cn.gif', '0', '15-05-2021 12:31:59') ;
#
# End of data contents of table ci_languages
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------


#
# Delete any existing table `ci_leads`
#

DROP TABLE IF EXISTS `ci_leads`;


#
# Table structure of table `ci_leads`
#

CREATE TABLE `ci_leads` (
  `lead_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `profile_photo` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `address_1` text,
  `address_2` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `country` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`lead_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;

#
# Data contents of table ci_leads (0 records)
#

#
# End of data contents of table ci_leads
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------


#
# Delete any existing table `ci_leads_followup`
#

DROP TABLE IF EXISTS `ci_leads_followup`;


#
# Table structure of table `ci_leads_followup`
#

CREATE TABLE `ci_leads_followup` (
  `followup_id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `next_followup` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`followup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

#
# Data contents of table ci_leads_followup (0 records)
#

#
# End of data contents of table ci_leads_followup
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------


#
# Delete any existing table `ci_leave_applications`
#

DROP TABLE IF EXISTS `ci_leave_applications`;


#
# Table structure of table `ci_leave_applications`
#

CREATE TABLE `ci_leave_applications` (
  `leave_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(222) NOT NULL,
  `leave_type_id` int(222) NOT NULL,
  `from_date` varchar(200) NOT NULL,
  `to_date` varchar(200) NOT NULL,
  `reason` mediumtext NOT NULL,
  `remarks` mediumtext,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_half_day` tinyint(1) DEFAULT NULL,
  `leave_attachment` varchar(255) DEFAULT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`leave_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_leave_applications (2 records)
#
 
INSERT INTO `ci_leave_applications` VALUES ('1', '2', '23', '123', '2021-09-21', '2021-09-22', 'Test Reason', 'Test Remarks', '2', '0', '', '17-09-2021 08:36:15') ; 
INSERT INTO `ci_leave_applications` VALUES ('2', '2', '25', '117', '2021-09-18', '2021-09-20', 'Sick Leave Application', 'Hi Sir,\r\nI am writing this application to bring it to you kind notice that I&#39;m down with high fever and stomach infection and have been advised by my doctor to take rest for 3 days.\r\n         I would be highly obliged if you could grant me a leave for three days. I will be available on call if needed..\r\n\r\nThank You.\r\nMohammad Faizan Sarwar\r\n', '3', '0', '', '18-09-2021 01:56:42') ;
#
# End of data contents of table ci_leave_applications
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------


#
# Delete any existing table `ci_meetings`
#

DROP TABLE IF EXISTS `ci_meetings`;


#
# Table structure of table `ci_meetings`
#

CREATE TABLE `ci_meetings` (
  `meeting_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` varchar(255) DEFAULT NULL,
  `meeting_title` varchar(255) NOT NULL,
  `meeting_date` varchar(255) NOT NULL,
  `meeting_time` varchar(255) NOT NULL,
  `meeting_room` varchar(255) NOT NULL,
  `meeting_note` mediumtext NOT NULL,
  `meeting_color` varchar(200) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`meeting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_meetings (0 records)
#

#
# End of data contents of table ci_meetings
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------


#
# Delete any existing table `ci_membership`
#

DROP TABLE IF EXISTS `ci_membership`;


#
# Table structure of table `ci_membership`
#

CREATE TABLE `ci_membership` (
  `membership_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` varchar(100) DEFAULT NULL,
  `membership_type` varchar(200) NOT NULL,
  `price` decimal(65,2) NOT NULL DEFAULT '0.00',
  `plan_duration` int(11) NOT NULL,
  `total_employees` int(11) NOT NULL DEFAULT '0',
  `description` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`membership_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_membership (1 records)
#
 
INSERT INTO `ci_membership` VALUES ('6', '100585963', 'Pro Plan', '59.00', '2', '10', 'Free server unlimited approx 255k+ Premium collection', '09-05-2021 06:36:23') ;
#
# End of data contents of table ci_membership
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------


#
# Delete any existing table `ci_office_shifts`
#

DROP TABLE IF EXISTS `ci_office_shifts`;


#
# Table structure of table `ci_office_shifts`
#

CREATE TABLE `ci_office_shifts` (
  `office_shift_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `shift_name` varchar(255) NOT NULL,
  `monday_in_time` varchar(222) NOT NULL,
  `monday_out_time` varchar(222) NOT NULL,
  `tuesday_in_time` varchar(222) NOT NULL,
  `tuesday_out_time` varchar(222) NOT NULL,
  `wednesday_in_time` varchar(222) NOT NULL,
  `wednesday_out_time` varchar(222) NOT NULL,
  `thursday_in_time` varchar(222) NOT NULL,
  `thursday_out_time` varchar(222) NOT NULL,
  `friday_in_time` varchar(222) NOT NULL,
  `friday_out_time` varchar(222) NOT NULL,
  `saturday_in_time` varchar(222) NOT NULL,
  `saturday_out_time` varchar(222) NOT NULL,
  `sunday_in_time` varchar(222) NOT NULL,
  `sunday_out_time` varchar(222) NOT NULL,
  `created_at` varchar(222) NOT NULL,
  PRIMARY KEY (`office_shift_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_office_shifts (2 records)
#
 
INSERT INTO `ci_office_shifts` VALUES ('1', '2', 'Day Shift', '10:00', '19:00', '10:00', '19:00', '10:00', '19:00', '10:00', '19:00', '10:00', '19:00', '10:00', '19:00', '', '', '20-07-2021 05:27:53') ; 
INSERT INTO `ci_office_shifts` VALUES ('2', '2', 'Night Shift', '18:30', '03:30', '18:30', '03:30', '18:30', '03:30', '18:30', '03:30', '18:30', '03:30', '', '', '', '', '20-07-2021 05:32:01') ;
#
# End of data contents of table ci_office_shifts
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------


#
# Delete any existing table `ci_official_documents`
#

DROP TABLE IF EXISTS `ci_official_documents`;


#
# Table structure of table `ci_official_documents`
#

CREATE TABLE `ci_official_documents` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `license_name` varchar(255) NOT NULL,
  `document_type` varchar(255) NOT NULL,
  `license_no` varchar(200) DEFAULT NULL,
  `expiry_date` varchar(200) DEFAULT NULL,
  `document_file` varchar(255) NOT NULL,
  `created_at` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_official_documents (0 records)
#

#
# End of data contents of table ci_official_documents
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------


#
# Delete any existing table `ci_payslip_allowances`
#

DROP TABLE IF EXISTS `ci_payslip_allowances`;


#
# Table structure of table `ci_payslip_allowances`
#

CREATE TABLE `ci_payslip_allowances` (
  `payslip_allowances_id` int(11) NOT NULL AUTO_INCREMENT,
  `payslip_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `is_taxable` int(11) NOT NULL,
  `is_fixed` int(11) NOT NULL,
  `pay_title` varchar(200) NOT NULL,
  `pay_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`payslip_allowances_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_payslip_allowances (0 records)
#

#
# End of data contents of table ci_payslip_allowances
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------


#
# Delete any existing table `ci_payslip_commissions`
#

DROP TABLE IF EXISTS `ci_payslip_commissions`;


#
# Table structure of table `ci_payslip_commissions`
#

CREATE TABLE `ci_payslip_commissions` (
  `payslip_commissions_id` int(11) NOT NULL AUTO_INCREMENT,
  `payslip_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `is_taxable` int(11) NOT NULL,
  `is_fixed` int(11) NOT NULL,
  `pay_title` varchar(200) NOT NULL,
  `pay_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`payslip_commissions_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_payslip_commissions (0 records)
#

#
# End of data contents of table ci_payslip_commissions
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------


#
# Delete any existing table `ci_payslip_other_payments`
#

DROP TABLE IF EXISTS `ci_payslip_other_payments`;


#
# Table structure of table `ci_payslip_other_payments`
#

CREATE TABLE `ci_payslip_other_payments` (
  `payslip_other_payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payslip_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `is_taxable` int(11) NOT NULL,
  `is_fixed` int(11) NOT NULL,
  `pay_title` varchar(200) NOT NULL,
  `pay_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`payslip_other_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_payslip_other_payments (0 records)
#

#
# End of data contents of table ci_payslip_other_payments
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------


#
# Delete any existing table `ci_payslip_statutory_deductions`
#

DROP TABLE IF EXISTS `ci_payslip_statutory_deductions`;


#
# Table structure of table `ci_payslip_statutory_deductions`
#

CREATE TABLE `ci_payslip_statutory_deductions` (
  `payslip_deduction_id` int(11) NOT NULL AUTO_INCREMENT,
  `payslip_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `is_fixed` int(11) NOT NULL,
  `pay_title` varchar(200) NOT NULL,
  `pay_amount` decimal(65,2) NOT NULL DEFAULT '0.00',
  `salary_month` varchar(200) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`payslip_deduction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_payslip_statutory_deductions (2 records)
#
 
INSERT INTO `ci_payslip_statutory_deductions` VALUES ('1', '1', '12', '1', 'EPF', '1800.00', '2021-07', '21-07-2021 05:48:32') ; 
INSERT INTO `ci_payslip_statutory_deductions` VALUES ('2', '1', '12', '1', 'ESI', '113.00', '2021-07', '21-07-2021 05:48:32') ;
#
# End of data contents of table ci_payslip_statutory_deductions
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------


#
# Delete any existing table `ci_payslips`
#

DROP TABLE IF EXISTS `ci_payslips`;


#
# Table structure of table `ci_payslips`
#

CREATE TABLE `ci_payslips` (
  `payslip_id` int(11) NOT NULL AUTO_INCREMENT,
  `payslip_key` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `salary_month` varchar(200) NOT NULL,
  `wages_type` int(11) NOT NULL,
  `payslip_type` varchar(50) NOT NULL,
  `basic_salary` decimal(65,2) NOT NULL DEFAULT '0.00',
  `daily_wages` decimal(65,2) NOT NULL DEFAULT '0.00',
  `hours_worked` varchar(50) NOT NULL DEFAULT '0',
  `total_allowances` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_commissions` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_statutory_deductions` decimal(65,2) NOT NULL DEFAULT '0.00',
  `total_other_payments` decimal(65,2) NOT NULL DEFAULT '0.00',
  `net_salary` decimal(65,2) NOT NULL DEFAULT '0.00',
  `payment_method` int(11) NOT NULL,
  `pay_comments` mediumtext NOT NULL,
  `is_payment` int(11) NOT NULL,
  `year_to_date` varchar(200) NOT NULL,
  `is_advance_salary_deduct` int(11) NOT NULL,
  `advance_salary_amount` decimal(65,2) DEFAULT NULL,
  `is_loan_deduct` int(11) NOT NULL,
  `loan_amount` decimal(65,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`payslip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_payslips (1 records)
#
 
INSERT INTO `ci_payslips` VALUES ('1', 'IfoT2FTPzlj65pOW_WgnjRBpv59MAnr83SLYzoU3t4w', '2', '12', '2021-05', '1', 'full_monthly', '15000.00', '0.00', '0', '0.00', '0.00', '1913.00', '0.00', '13087.00', '1', 'Paid via NEFT', '1', '21-07-2021', '0', '0.00', '0', '0.00', '0', '21-07-2021') ;
#
# End of data contents of table ci_payslips
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------


#
# Delete any existing table `ci_performance_appraisal`
#

DROP TABLE IF EXISTS `ci_performance_appraisal`;


#
# Table structure of table `ci_performance_appraisal`
#

CREATE TABLE `ci_performance_appraisal` (
  `performance_appraisal_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `appraisal_year_month` varchar(255) NOT NULL,
  `remarks` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`performance_appraisal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_performance_appraisal (0 records)
#

#
# End of data contents of table ci_performance_appraisal
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------


#
# Delete any existing table `ci_performance_appraisal_options`
#

DROP TABLE IF EXISTS `ci_performance_appraisal_options`;


#
# Table structure of table `ci_performance_appraisal_options`
#

CREATE TABLE `ci_performance_appraisal_options` (
  `performance_appraisal_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `appraisal_id` int(11) NOT NULL,
  `appraisal_type` varchar(200) NOT NULL,
  `appraisal_option_id` int(11) NOT NULL,
  `appraisal_option_value` int(11) NOT NULL,
  PRIMARY KEY (`performance_appraisal_options_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_performance_appraisal_options (0 records)
#

#
# End of data contents of table ci_performance_appraisal_options
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------


#
# Delete any existing table `ci_performance_indicator`
#

DROP TABLE IF EXISTS `ci_performance_indicator`;


#
# Table structure of table `ci_performance_indicator`
#

CREATE TABLE `ci_performance_indicator` (
  `performance_indicator_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `designation_id` int(111) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`performance_indicator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_performance_indicator (1 records)
#
 
INSERT INTO `ci_performance_indicator` VALUES ('1', '2', 'Performance for 20-21', '2', '2', '21-07-2021 03:57:54') ;
#
# End of data contents of table ci_performance_indicator
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------


#
# Delete any existing table `ci_performance_indicator_options`
#

DROP TABLE IF EXISTS `ci_performance_indicator_options`;


#
# Table structure of table `ci_performance_indicator_options`
#

CREATE TABLE `ci_performance_indicator_options` (
  `performance_indicator_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `indicator_type` varchar(200) NOT NULL,
  `indicator_option_id` int(11) NOT NULL,
  `indicator_option_value` int(11) NOT NULL,
  PRIMARY KEY (`performance_indicator_options_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_performance_indicator_options (10 records)
#
 
INSERT INTO `ci_performance_indicator_options` VALUES ('1', '2', '1', 'technical', '101', '4') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('2', '2', '1', 'technical', '102', '4') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('3', '2', '1', 'technical', '103', '5') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('4', '2', '1', 'technical', '104', '4') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('5', '2', '1', 'technical', '105', '5') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('6', '2', '1', 'organizational', '106', '4') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('7', '2', '1', 'organizational', '107', '4') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('8', '2', '1', 'organizational', '108', '4') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('9', '2', '1', 'organizational', '109', '5') ; 
INSERT INTO `ci_performance_indicator_options` VALUES ('10', '2', '1', 'organizational', '110', '5') ;
#
# End of data contents of table ci_performance_indicator_options
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------


#
# Delete any existing table `ci_policies`
#

DROP TABLE IF EXISTS `ci_policies`;


#
# Table structure of table `ci_policies`
#

CREATE TABLE `ci_policies` (
  `policy_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(111) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`policy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_policies (0 records)
#

#
# End of data contents of table ci_policies
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------


#
# Delete any existing table `ci_projects`
#

DROP TABLE IF EXISTS `ci_projects`;


#
# Table structure of table `ci_projects`
#

CREATE TABLE `ci_projects` (
  `project_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `assigned_to` mediumtext,
  `associated_goals` text,
  `priority` varchar(255) NOT NULL,
  `project_no` varchar(255) DEFAULT NULL,
  `budget_hours` varchar(255) DEFAULT NULL,
  `summary` mediumtext NOT NULL,
  `description` mediumtext,
  `project_progress` varchar(255) NOT NULL,
  `project_note` longtext,
  `status` tinyint(2) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_projects (0 records)
#

#
# End of data contents of table ci_projects
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------


#
# Delete any existing table `ci_projects_bugs`
#

DROP TABLE IF EXISTS `ci_projects_bugs`;


#
# Table structure of table `ci_projects_bugs`
#

CREATE TABLE `ci_projects_bugs` (
  `project_bug_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `bug_note` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`project_bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_projects_bugs (0 records)
#

#
# End of data contents of table ci_projects_bugs
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------


#
# Delete any existing table `ci_projects_discussion`
#

DROP TABLE IF EXISTS `ci_projects_discussion`;


#
# Table structure of table `ci_projects_discussion`
#

CREATE TABLE `ci_projects_discussion` (
  `project_discussion_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `discussion_text` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`project_discussion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_projects_discussion (0 records)
#

#
# End of data contents of table ci_projects_discussion
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------


#
# Delete any existing table `ci_projects_files`
#

DROP TABLE IF EXISTS `ci_projects_files`;


#
# Table structure of table `ci_projects_files`
#

CREATE TABLE `ci_projects_files` (
  `project_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `file_title` varchar(255) NOT NULL,
  `attachment_file` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`project_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_projects_files (0 records)
#

#
# End of data contents of table ci_projects_files
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------


#
# Delete any existing table `ci_projects_notes`
#

DROP TABLE IF EXISTS `ci_projects_notes`;


#
# Table structure of table `ci_projects_notes`
#

CREATE TABLE `ci_projects_notes` (
  `project_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `project_note` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`project_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_projects_notes (0 records)
#

#
# End of data contents of table ci_projects_notes
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------


#
# Delete any existing table `ci_projects_timelogs`
#

DROP TABLE IF EXISTS `ci_projects_timelogs`;


#
# Table structure of table `ci_projects_timelogs`
#

CREATE TABLE `ci_projects_timelogs` (
  `timelogs_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `total_hours` varchar(255) NOT NULL,
  `timelogs_memo` text NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`timelogs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

#
# Data contents of table ci_projects_timelogs (0 records)
#

#
# End of data contents of table ci_projects_timelogs
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------


#
# Delete any existing table `ci_rec_candidates`
#

DROP TABLE IF EXISTS `ci_rec_candidates`;


#
# Table structure of table `ci_rec_candidates`
#

CREATE TABLE `ci_rec_candidates` (
  `candidate_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `job_id` int(111) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `message` mediumtext NOT NULL,
  `job_resume` mediumtext NOT NULL,
  `application_status` int(11) NOT NULL DEFAULT '0',
  `application_remarks` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`candidate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_rec_candidates (0 records)
#

#
# End of data contents of table ci_rec_candidates
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------


#
# Delete any existing table `ci_rec_interviews`
#

DROP TABLE IF EXISTS `ci_rec_interviews`;


#
# Table structure of table `ci_rec_interviews`
#

CREATE TABLE `ci_rec_interviews` (
  `job_interview_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `job_id` int(111) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `staff_id` varchar(11) NOT NULL,
  `interview_place` varchar(255) NOT NULL,
  `interview_date` varchar(255) NOT NULL,
  `interview_time` varchar(255) NOT NULL,
  `interviewer_id` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `interview_remarks` text,
  `status` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`job_interview_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_rec_interviews (0 records)
#

#
# End of data contents of table ci_rec_interviews
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------


#
# Delete any existing table `ci_rec_jobs`
#

DROP TABLE IF EXISTS `ci_rec_jobs`;


#
# Table structure of table `ci_rec_jobs`
#

CREATE TABLE `ci_rec_jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `designation_id` int(111) NOT NULL,
  `job_type` int(225) NOT NULL,
  `job_vacancy` int(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `minimum_experience` varchar(255) NOT NULL,
  `date_of_closing` varchar(200) NOT NULL,
  `short_description` mediumtext NOT NULL,
  `long_description` mediumtext NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_rec_jobs (0 records)
#

#
# End of data contents of table ci_rec_jobs
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------


#
# Delete any existing table `ci_recent_activity`
#

DROP TABLE IF EXISTS `ci_recent_activity`;


#
# Table structure of table `ci_recent_activity`
#

CREATE TABLE `ci_recent_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_type` varchar(200) NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_recent_activity (0 records)
#

#
# End of data contents of table ci_recent_activity
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------


#
# Delete any existing table `ci_resignations`
#

DROP TABLE IF EXISTS `ci_resignations`;


#
# Table structure of table `ci_resignations`
#

CREATE TABLE `ci_resignations` (
  `resignation_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `notice_date` varchar(255) NOT NULL,
  `resignation_date` varchar(255) NOT NULL,
  `reason` mediumtext NOT NULL,
  `added_by` int(111) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`resignation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_resignations (0 records)
#

#
# End of data contents of table ci_resignations
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------


#
# Delete any existing table `ci_staff_roles`
#

DROP TABLE IF EXISTS `ci_staff_roles`;


#
# Table structure of table `ci_staff_roles`
#

CREATE TABLE `ci_staff_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `role_name` varchar(200) NOT NULL,
  `role_access` varchar(200) NOT NULL,
  `role_resources` longtext NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_staff_roles (1 records)
#
 
INSERT INTO `ci_staff_roles` VALUES ('1', '2', 'Employee', '2', '0,attendance,pay_history,advance_salary1,advance_salary2,advance_salary3,training2,training5,training7,training_skill1,training_calendar,award1,award_type1,leave2,leave3,leave4,leave7,leave_calendar,leave_type1,leave_type2,leave_type3,complaint1,complaint2,resignation1,resignation2,hr_basic_info,hr_personal_info,hr_picture,hr_documents,change_password,ats2,policy5,org_chart,indicator1,tracking1,tracking2,tracking3,tracking5,track_calendar,hr_event1,events_calendar,holiday1,holidays_calendar,file1,file2,officialfile1,officialfile2,todo_ist,system_calendar', '21-07-2021 03:22:24') ;
#
# End of data contents of table ci_staff_roles
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------


#
# Delete any existing table `ci_support_ticket_files`
#

DROP TABLE IF EXISTS `ci_support_ticket_files`;


#
# Table structure of table `ci_support_ticket_files`
#

CREATE TABLE `ci_support_ticket_files` (
  `ticket_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `file_title` varchar(255) NOT NULL,
  `attachment_file` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`ticket_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_support_ticket_files (0 records)
#

#
# End of data contents of table ci_support_ticket_files
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------


#
# Delete any existing table `ci_support_ticket_notes`
#

DROP TABLE IF EXISTS `ci_support_ticket_notes`;


#
# Table structure of table `ci_support_ticket_notes`
#

CREATE TABLE `ci_support_ticket_notes` (
  `ticket_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `ticket_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `ticket_note` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`ticket_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_support_ticket_notes (0 records)
#

#
# End of data contents of table ci_support_ticket_notes
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------


#
# Delete any existing table `ci_support_ticket_reply`
#

DROP TABLE IF EXISTS `ci_support_ticket_reply`;


#
# Table structure of table `ci_support_ticket_reply`
#

CREATE TABLE `ci_support_ticket_reply` (
  `ticket_reply_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `ticket_id` int(111) NOT NULL,
  `sent_by` int(11) NOT NULL,
  `assign_to` int(11) NOT NULL,
  `reply_text` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`ticket_reply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_support_ticket_reply (0 records)
#

#
# End of data contents of table ci_support_ticket_reply
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------


#
# Delete any existing table `ci_support_tickets`
#

DROP TABLE IF EXISTS `ci_support_tickets`;


#
# Table structure of table `ci_support_tickets`
#

CREATE TABLE `ci_support_tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `ticket_code` varchar(200) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `ticket_priority` varchar(255) NOT NULL,
  `department_id` int(111) NOT NULL,
  `description` mediumtext NOT NULL,
  `ticket_remarks` mediumtext,
  `ticket_status` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_support_tickets (0 records)
#

#
# End of data contents of table ci_support_tickets
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------


#
# Delete any existing table `ci_system_documents`
#

DROP TABLE IF EXISTS `ci_system_documents`;


#
# Table structure of table `ci_system_documents`
#

CREATE TABLE `ci_system_documents` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `document_type` varchar(255) NOT NULL,
  `document_file` varchar(255) NOT NULL,
  `created_at` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_system_documents (0 records)
#

#
# End of data contents of table ci_system_documents
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------


#
# Delete any existing table `ci_tasks`
#

DROP TABLE IF EXISTS `ci_tasks`;


#
# Table structure of table `ci_tasks`
#

CREATE TABLE `ci_tasks` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `project_id` int(111) NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `assigned_to` varchar(255) DEFAULT NULL,
  `associated_goals` text,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `task_hour` varchar(200) DEFAULT NULL,
  `task_progress` varchar(200) DEFAULT NULL,
  `summary` text NOT NULL,
  `description` mediumtext,
  `task_status` int(5) NOT NULL,
  `task_note` mediumtext,
  `created_by` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_tasks (0 records)
#

#
# End of data contents of table ci_tasks
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------


#
# Delete any existing table `ci_tasks_discussion`
#

DROP TABLE IF EXISTS `ci_tasks_discussion`;


#
# Table structure of table `ci_tasks_discussion`
#

CREATE TABLE `ci_tasks_discussion` (
  `task_discussion_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `discussion_text` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`task_discussion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_tasks_discussion (0 records)
#

#
# End of data contents of table ci_tasks_discussion
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------


#
# Delete any existing table `ci_tasks_files`
#

DROP TABLE IF EXISTS `ci_tasks_files`;


#
# Table structure of table `ci_tasks_files`
#

CREATE TABLE `ci_tasks_files` (
  `task_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `file_title` varchar(255) NOT NULL,
  `attachment_file` mediumtext NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`task_file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_tasks_files (0 records)
#

#
# End of data contents of table ci_tasks_files
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------


#
# Delete any existing table `ci_tasks_notes`
#

DROP TABLE IF EXISTS `ci_tasks_notes`;


#
# Table structure of table `ci_tasks_notes`
#

CREATE TABLE `ci_tasks_notes` (
  `task_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `task_id` int(111) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `task_note` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`task_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_tasks_notes (0 records)
#

#
# End of data contents of table ci_tasks_notes
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------


#
# Delete any existing table `ci_timesheet`
#

DROP TABLE IF EXISTS `ci_timesheet`;


#
# Table structure of table `ci_timesheet`
#

CREATE TABLE `ci_timesheet` (
  `time_attendance_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `attendance_date` varchar(255) NOT NULL,
  `clock_in` varchar(255) NOT NULL,
  `clock_out` varchar(255) NOT NULL,
  `total_work` varchar(255) NOT NULL,
  `clock_in_ip_address` varchar(255) NOT NULL,
  `clock_out_ip_address` varchar(255) NOT NULL,
  `clock_in_out` varchar(255) NOT NULL,
  `clock_in_latitude` varchar(150) NOT NULL,
  `clock_in_longitude` varchar(150) NOT NULL,
  `clock_out_latitude` varchar(150) NOT NULL,
  `clock_out_longitude` varchar(150) NOT NULL,
  `time_late` varchar(255) NOT NULL,
  `early_leaving` varchar(255) NOT NULL,
  `overtime` varchar(255) NOT NULL,
  `total_rest` varchar(255) NOT NULL,
  `attendance_status` varchar(100) NOT NULL,
  PRIMARY KEY (`time_attendance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_timesheet (92 records)
#
 
INSERT INTO `ci_timesheet` VALUES ('1', '2', '5', '2021-07-27', '2021-07-27 02:07:13', '2021-07-27 03:49:09', '1:41', '122.172.1.39', '122.172.1.39', '0', '12.9715987', '77.5945627', '12.9715987', '77.5945627', '2021-07-27 02:07:13', '2021-07-27 03:49:09', '2021-07-27 03:49:09', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('3', '2', '3', '2021-07-27', '2021-07-27 18:39:00', '2021-07-27 03:40:00', '14:59', '122.172.221.181', '122.172.221.181', '0', '13.3192031', '77.1326565', '1', '1', '2021-07-27 18:39:00', '2021-07-27 03:40:00', '2021-07-27 03:40:00', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('4', '2', '5', '2021-07-27', '2021-07-27 18:39:56', '2021-07-27 18:41:09', '0:1', '122.167.176.52', '122.167.176.52', '0', '13.324087', '77.0962529', '13.324087', '77.0962529', '2021-07-27 18:39:56', '2021-07-27 18:41:09', '2021-07-27 18:41:09', '14:50', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('6', '2', '5', '2021-07-28', '2021-07-28 03:46:58', '2021-07-28 03:47:13', '0:0', '122.167.176.52', '122.167.176.52', '0', '13.0123851', '77.74405689999999', '13.0123851', '77.74405689999999', '2021-07-28 03:46:58', '2021-07-28 03:47:13', '2021-07-28 03:47:13', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('7', '2', '13', '2021-07-28', '2021-07-28 09:56:15', '2021-07-28 19:00:46', '9:4', '157.45.16.74', '157.45.29.13', '0', '12.9568', '77.5833', '12.9568', '77.5833', '2021-07-28 09:56:15', '2021-07-28 19:00:46', '2021-07-28 19:00:46', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('8', '2', '4', '2021-07-28', '2021-07-28 18:25:08', '', '00:00', '122.172.221.181', '1', '0', '13.3191915', '77.13263839999999', '1', '1', '2021-07-28 18:25:08', '2021-07-28 18:25:08', '2021-07-28 18:25:08', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('10', '2', '9', '2021-07-28', '2021-07-28 20:18:45', '', '00:00', '122.172.76.20', '1', '0', '13.318946799999999', '77.1324419', '1', '1', '2021-07-28 20:18:45', '2021-07-28 20:18:45', '2021-07-28 20:18:45', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('11', '2', '13', '2021-07-29', '2021-07-29 09:56:04', '', '00:00', '157.45.251.173', '1', '0', '12.9794048', '77.594624', '1', '1', '2021-07-29 09:56:04', '2021-07-29 09:56:04', '2021-07-29 09:56:04', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('12', '2', '10', '2021-07-29', '2021-07-29 11:09:19', '', '00:00', '122.172.150.121', '1', '0', '13.33497', '77.125969', '1', '1', '2021-07-29 11:09:19', '2021-07-29 11:09:19', '2021-07-29 11:09:19', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('13', '2', '9', '2021-07-29', '2021-07-29 18:29:00', '2021-07-29 18:34:06', '0:5', '122.171.28.140', '122.171.28.140', '0', '13.318951799999999', '77.1324157', '13.3189668', '77.1324262', '2021-07-29 18:29:00', '2021-07-29 18:34:06', '2021-07-29 18:34:06', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('14', '2', '9', '2021-07-29', '2021-07-29 18:34:10', '', '00:00', '122.171.28.140', '1', '0', '13.3189668', '77.1324262', '1', '1', '2021-07-29 18:34:10', '2021-07-29 18:34:10', '2021-07-29 18:34:10', '0:0', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('15', '2', '4', '2021-07-29', '2021-07-29 18:35:12', '', '00:00', '122.172.221.181', '1', '0', '12.9990656', '77.6208384', '1', '1', '2021-07-29 18:35:12', '2021-07-29 18:35:12', '2021-07-29 18:35:12', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('17', '2', '5', '2021-07-30', '2021-07-30 03:20:14', '2021-07-30 03:21:55', '0:1', '122.167.27.196', '122.167.27.196', '0', '12.9007616', '77.6634368', '12.9007616', '77.6634368', '2021-07-30 03:20:14', '2021-07-30 03:21:55', '2021-07-30 03:21:55', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('18', '2', '10', '2021-07-30', '2021-07-30 09:37:32', '2021-07-30 19:46:55', '10:9', '122.171.125.0', '122.171.125.0', '0', '13.33497', '77.125969', '13.33497', '77.125969', '2021-07-30 09:37:32', '2021-07-30 19:46:55', '2021-07-30 19:46:55', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('19', '2', '13', '2021-07-30', '2021-07-30 10:38:30', '2021-07-30 19:51:27', '9:12', '157.45.95.234', '157.45.169.239', '0', '12.9568', '77.5833', '12.9568', '77.5833', '2021-07-30 10:38:30', '2021-07-30 19:51:27', '2021-07-30 19:51:27', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('20', '2', '9', '2021-07-30', '2021-07-30 18:38:54', '', '00:00', '122.172.211.193', '1', '0', '13.3189599', '77.132414', '1', '1', '2021-07-30 18:38:54', '2021-07-30 18:38:54', '2021-07-30 18:38:54', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('22', '2', '4', '2021-07-30', '2021-07-30 18:54:15', '', '00:00', '122.172.221.181', '1', '0', '13.3192037', '77.1326215', '1', '1', '2021-07-30 18:54:15', '2021-07-30 18:54:15', '2021-07-30 18:54:15', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('23', '2', '13', '2021-07-31', '2021-07-31 09:33:05', '', '00:00', '157.50.36.141', '1', '0', '12.9568', '77.5833', '1', '1', '2021-07-31 09:33:05', '2021-07-31 09:33:05', '2021-07-31 09:33:05', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('24', '2', '13', '2021-08-02', '2021-08-02 10:06:05', '', '00:00', '157.45.14.63', '1', '0', '12.956467199999999', '77.594624', '1', '1', '2021-08-02 10:06:05', '2021-08-02 10:06:05', '2021-08-02 10:06:05', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('25', '2', '5', '2021-08-02', '2021-08-02 18:36:42', '2021-09-10 18:23:43', '23:46', '122.172.92.77', '122.167.168.172', '0', '12.957203699999999', '77.61616769999999', '12.8827419', '77.6269985', '2021-08-02 18:36:42', '2021-09-10 18:23:43', '2021-09-10 18:23:43', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('26', '2', '9', '2021-08-02', '2021-08-02 19:58:56', '', '00:00', '122.167.21.163', '1', '0', '13.3189576', '77.1324482', '1', '1', '2021-08-02 19:58:56', '2021-08-02 19:58:56', '2021-08-02 19:58:56', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('27', '2', '13', '2021-08-03', '2021-08-03 09:44:25', '', '00:00', '157.45.228.58', '1', '0', '12.9794048', '77.594624', '1', '1', '2021-08-03 09:44:25', '2021-08-03 09:44:25', '2021-08-03 09:44:25', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('28', '2', '5', '2021-08-03', '2021-08-03 18:27:39', '2021-09-08 23:11:55', '0:0', '122.166.135.98', '103.88.59.67', '0', '12.9007616', '77.63394559999999', '25.6130754', '85.1412305', '2021-08-03 18:27:39', '2021-09-08 23:11:55', '2021-09-08 23:11:55', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('29', '2', '4', '2021-08-03', '2021-08-03 18:41:30', '', '00:00', '122.172.221.181', '1', '0', '13.319203', '77.1326558', '1', '1', '2021-08-03 18:41:30', '2021-08-03 18:41:30', '2021-08-03 18:41:30', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('30', '2', '9', '2021-08-03', '2021-08-03 18:57:01', '', '00:00', '122.171.81.16', '1', '0', '13.3189596', '77.1324272', '1', '1', '2021-08-03 18:57:01', '2021-08-03 18:57:01', '2021-08-03 18:57:01', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('31', '2', '3', '2021-08-02', '2021-08-02 18:30:00', '2021-08-02 03:45:00', '14:45', '1', '1', '0', '1', '1', '1', '1', '2021-08-02 18:30:00', '2021-08-02 03:45:00', '2021-08-02 03:45:00', '0', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('32', '2', '13', '2021-08-04', '2021-08-04 09:32:59', '', '00:00', '157.50.22.232', '1', '0', '12.956467199999999', '77.594624', '1', '1', '2021-08-04 09:32:59', '2021-08-04 09:32:59', '2021-08-04 09:32:59', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('33', '2', '5', '2021-08-04', '2021-08-04 18:32:33', '2021-09-08 23:09:32', '0:0', '122.171.89.54', '122.167.134.205', '0', '12.9164243', '77.60609680000002', '13.0293128', '77.7108574', '2021-08-04 18:32:33', '2021-09-08 23:09:32', '2021-09-08 23:09:32', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('34', '2', '9', '2021-08-04', '2021-08-04 18:49:39', '', '00:00', '117.96.241.230', '1', '0', '13.318943599999999', '77.13244329999999', '1', '1', '2021-08-04 18:49:39', '2021-08-04 18:49:39', '2021-08-04 18:49:39', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('35', '2', '5', '2021-08-05', '2021-08-05 18:39:56', '2021-09-08 23:08:38', '0:0', '122.172.10.209', '122.167.134.205', '0', '13.064103699999999', '77.5931209', '13.0293128', '77.7108574', '2021-08-05 18:39:56', '2021-09-08 23:08:38', '2021-09-08 23:08:38', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('36', '2', '9', '2021-08-05', '2021-08-05 19:00:28', '', '00:00', '122.166.142.184', '1', '0', '13.3189472', '77.1324492', '1', '1', '2021-08-05 19:00:28', '2021-08-05 19:00:28', '2021-08-05 19:00:28', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('37', '2', '13', '2021-08-06', '2021-08-06 10:03:39', '', '00:00', '157.45.196.13', '1', '0', '12.9794048', '77.594624', '1', '1', '2021-08-06 10:03:39', '2021-08-06 10:03:39', '2021-08-06 10:03:39', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('38', '2', '9', '2021-08-06', '2021-08-06 18:46:07', '', '00:00', '171.61.117.77', '1', '0', '13.3189058', '77.1324701', '1', '1', '2021-08-06 18:46:07', '2021-08-06 18:46:07', '2021-08-06 18:46:07', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('39', '2', '13', '2021-08-07', '2021-08-07 10:09:16', '', '00:00', '157.50.40.190', '1', '0', '12.9794048', '77.594624', '1', '1', '2021-08-07 10:09:16', '2021-08-07 10:09:16', '2021-08-07 10:09:16', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('40', '2', '9', '2021-08-09', '2021-08-09 18:33:38', '', '00:00', '122.171.81.119', '1', '0', '13.318958499999999', '77.1324431', '1', '1', '2021-08-09 18:33:38', '2021-08-09 18:33:38', '2021-08-09 18:33:38', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('41', '2', '5', '2021-08-09', '2021-08-09 18:49:33', '2021-09-08 23:08:34', '0:0', '223.186.207.96', '122.167.134.205', '0', '13.3240746', '77.0961645', '13.0293128', '77.7108574', '2021-08-09 18:49:33', '2021-09-08 23:08:34', '2021-09-08 23:08:34', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('42', '2', '5', '2021-08-10', '2021-08-10 18:43:41', '2021-09-08 23:08:26', '0:0', '122.172.123.86', '122.167.134.205', '0', '12.2806862', '76.6297076', '13.0293128', '77.7108574', '2021-08-10 18:43:41', '2021-09-08 23:08:26', '2021-09-08 23:08:26', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('43', '2', '4', '2021-08-10', '2021-08-10 18:52:25', '', '00:00', '122.171.85.161', '1', '0', '13.3192085', '77.13265229999999', '1', '1', '2021-08-10 18:52:25', '2021-08-10 18:52:25', '2021-08-10 18:52:25', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('44', '2', '9', '2021-08-10', '2021-08-10 19:57:00', '', '00:00', '122.167.210.148', '1', '0', '13.318937499999999', '77.13243299999999', '1', '1', '2021-08-10 19:57:00', '2021-08-10 19:57:00', '2021-08-10 19:57:00', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('45', '2', '13', '2021-08-11', '2021-08-11 09:39:26', '2021-08-11 21:45:56', '12:6', '223.186.211.190', '157.45.174.69', '0', '13.9259', '75.6078', '12.9531904', '77.594624', '2021-08-11 09:39:26', '2021-08-11 21:45:56', '2021-08-11 21:45:56', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('46', '2', '9', '2021-08-11', '2021-08-11 18:32:59', '', '00:00', '122.172.173.143', '1', '0', '13.3189273', '77.1324687', '1', '1', '2021-08-11 18:32:59', '2021-08-11 18:32:59', '2021-08-11 18:32:59', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('47', '2', '4', '2021-08-12', '2021-08-12 18:39:16', '', '00:00', '122.171.85.161', '1', '0', '13.319200299999999', '77.1326474', '1', '1', '2021-08-12 18:39:16', '2021-08-12 18:39:16', '2021-08-12 18:39:16', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('48', '2', '9', '2021-08-12', '2021-08-12 19:13:24', '', '00:00', '122.171.126.27', '1', '0', '13.3189214', '77.1324571', '1', '1', '2021-08-12 19:13:24', '2021-08-12 19:13:24', '2021-08-12 19:13:24', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('49', '2', '9', '2021-08-13', '2021-08-13 18:49:36', '', '00:00', '122.171.126.27', '1', '0', '13.3189262', '77.132471', '1', '1', '2021-08-13 18:49:36', '2021-08-13 18:49:36', '2021-08-13 18:49:36', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('50', '2', '4', '2021-08-13', '2021-08-13 19:09:13', '', '00:00', '122.171.82.104', '1', '0', '13.319200600000002', '77.13264749999999', '1', '1', '2021-08-13 19:09:13', '2021-08-13 19:09:13', '2021-08-13 19:09:13', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('51', '2', '9', '2021-08-16', '2021-08-16 19:50:29', '', '00:00', '122.167.129.155', '1', '0', '13.3189607', '77.1324736', '1', '1', '2021-08-16 19:50:29', '2021-08-16 19:50:29', '2021-08-16 19:50:29', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('52', '2', '13', '2021-08-17', '2021-08-17 13:43:52', '', '00:00', '157.45.173.105', '1', '0', '12.9794048', '77.594624', '1', '1', '2021-08-17 13:43:52', '2021-08-17 13:43:52', '2021-08-17 13:43:52', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('53', '2', '9', '2021-08-17', '2021-08-17 18:38:19', '', '00:00', '122.167.129.155', '1', '0', '13.318939799999999', '77.1324618', '1', '1', '2021-08-17 18:38:19', '2021-08-17 18:38:19', '2021-08-17 18:38:19', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('54', '2', '9', '2021-08-18', '2021-08-18 18:43:27', '', '00:00', '122.167.129.155', '1', '0', '13.3189767', '77.1324808', '1', '1', '2021-08-18 18:43:27', '2021-08-18 18:43:27', '2021-08-18 18:43:27', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('55', '2', '9', '2021-08-19', '2021-08-19 18:00:11', '', '00:00', '122.167.129.155', '1', '0', '13.3189765', '77.1324888', '1', '1', '2021-08-19 18:00:11', '2021-08-19 18:00:11', '2021-08-19 18:00:11', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('56', '2', '4', '2021-08-19', '2021-08-19 18:41:17', '', '00:00', '122.167.27.188', '1', '0', '13.3192416', '77.1326426', '1', '1', '2021-08-19 18:41:17', '2021-08-19 18:41:17', '2021-08-19 18:41:17', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('57', '2', '5', '2021-08-20', '2021-08-20 18:39:22', '2021-09-08 23:08:22', '0:0', '122.171.67.10', '122.167.134.205', '0', '12.920033799999999', '77.5461674', '13.0293128', '77.7108574', '2021-08-20 18:39:22', '2021-09-08 23:08:22', '2021-09-08 23:08:22', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('58', '2', '9', '2021-08-20', '2021-08-20 18:41:00', '', '00:00', '122.172.168.58', '1', '0', '13.3189467', '77.132484', '1', '1', '2021-08-20 18:41:00', '2021-08-20 18:41:00', '2021-08-20 18:41:00', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('59', '2', '4', '2021-08-20', '2021-08-20 19:50:40', '', '00:00', '122.167.27.188', '1', '0', '13.3192161', '77.1327134', '1', '1', '2021-08-20 19:50:40', '2021-08-20 19:50:40', '2021-08-20 19:50:40', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('60', '2', '4', '2021-08-23', '2021-08-23 18:30:47', '', '00:00', '171.61.125.177', '1', '0', '13.3192396', '77.1326386', '1', '1', '2021-08-23 18:30:47', '2021-08-23 18:30:47', '2021-08-23 18:30:47', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('61', '2', '4', '2021-08-25', '2021-08-25 18:41:36', '', '00:00', '171.61.125.177', '1', '0', '13.319233800000001', '77.1326461', '1', '1', '2021-08-25 18:41:36', '2021-08-25 18:41:36', '2021-08-25 18:41:36', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('62', '2', '9', '2021-08-25', '2021-08-25 21:12:39', '', '00:00', '122.171.74.17', '1', '0', '13.3189811', '77.132509', '1', '1', '2021-08-25 21:12:39', '2021-08-25 21:12:39', '2021-08-25 21:12:39', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('63', '2', '9', '2021-08-26', '2021-08-26 16:41:42', '', '00:00', '122.167.169.226', '1', '0', '13.318977199999999', '77.13250599999999', '1', '1', '2021-08-26 16:41:42', '2021-08-26 16:41:42', '2021-08-26 16:41:42', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('64', '2', '4', '2021-08-27', '2021-08-27 18:48:05', '', '00:00', '171.61.125.177', '1', '0', '13.319248199999999', '77.1326332', '1', '1', '2021-08-27 18:48:05', '2021-08-27 18:48:05', '2021-08-27 18:48:05', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('65', '2', '9', '2021-08-27', '2021-08-27 20:00:19', '2021-08-27 22:25:55', '2:25', '122.167.235.194', '122.167.235.194', '0', '13.3189716', '77.1324704', '13.3189835', '77.13245239999999', '2021-08-27 20:00:19', '2021-08-27 22:25:55', '2021-08-27 22:25:55', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('66', '2', '9', '2021-08-27', '2021-08-27 22:26:34', '', '00:00', '122.167.235.194', '1', '0', '13.3189835', '77.13245239999999', '1', '1', '2021-08-27 22:26:34', '2021-08-27 22:26:34', '2021-08-27 22:26:34', '0:0', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('67', '2', '9', '2021-08-30', '2021-08-30 15:58:45', '', '00:00', '122.167.210.209', '1', '0', '13.3189564', '77.1324795', '1', '1', '2021-08-30 15:58:45', '2021-08-30 15:58:45', '2021-08-30 15:58:45', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('68', '2', '9', '2021-08-31', '2021-08-31 18:34:55', '', '00:00', '122.171.81.61', '1', '0', '13.318986599999999', '77.1324595', '1', '1', '2021-08-31 18:34:55', '2021-08-31 18:34:55', '2021-08-31 18:34:55', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('69', '2', '5', '2021-08-31', '2021-08-31 18:47:19', '2021-09-08 23:08:18', '0:0', '122.172.126.145', '122.167.134.205', '0', '12.9859584', '77.6077312', '13.0293128', '77.7108574', '2021-08-31 18:47:19', '2021-09-08 23:08:18', '2021-09-08 23:08:18', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('70', '2', '9', '2021-09-01', '2021-09-01 17:33:44', '', '00:00', '117.96.243.130', '1', '0', '13.318970799999999', '77.13246649999999', '1', '1', '2021-09-01 17:33:44', '2021-09-01 17:33:44', '2021-09-01 17:33:44', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('71', '2', '4', '2021-09-01', '2021-09-01 18:29:09', '', '00:00', '122.172.215.26', '1', '0', '13.319248499999999', '77.1326334', '1', '1', '2021-09-01 18:29:09', '2021-09-01 18:29:09', '2021-09-01 18:29:09', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('72', '2', '13', '2021-09-02', '2021-09-02 13:17:20', '', '00:00', '106.193.36.152', '1', '0', '15.3172775', '75.7138884', '1', '1', '2021-09-02 13:17:20', '2021-09-02 13:17:20', '2021-09-02 13:17:20', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('73', '2', '9', '2021-09-02', '2021-09-02 17:27:04', '', '00:00', '117.96.243.130', '1', '0', '13.3189493', '77.1324823', '1', '1', '2021-09-02 17:27:04', '2021-09-02 17:27:04', '2021-09-02 17:27:04', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('74', '2', '5', '2021-09-02', '2021-09-02 18:38:52', '2021-09-03 23:06:58', '0:0', '122.172.219.129', '122.167.134.205', '0', '12.9073152', '77.5749632', '13.0293128', '77.7108574', '2021-09-02 18:38:52', '2021-09-08 23:06:58', '2021-09-08 23:06:58', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('76', '2', '25', '2021-09-07', '2021-09-07 11:03:34', '2021-09-07 19:23:06', '7:20', '223.230.171.14', '223.230.171.14', '0', '25.6179869', '85.1736956', '25.6128461', '85.140968', '2021-09-07 11:43:34', '2021-09-07 19:23:06', '2021-09-07 19:23:06', '0:0', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('80', '2', '24', '2021-09-07', '2021-09-07 21:35:16', '2021-09-08 21:37:35', '0:0', '103.88.59.67', '103.88.59.67', '0', '25.613025099999998', '85.14122760000001', '25.6130563', '85.1412245', '2021-09-07 21:35:16', '2021-09-08 21:37:35', '2021-09-08 21:37:35', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('81', '2', '25', '2021-09-08', '2021-09-08 10:34:46', '2021-09-08 18:44:11', '8:11', '223.230.171.14', '103.88.59.67', '0', '25.6171101', '85.1694842', '25.6130754', '85.1412305', '2021-09-08 17:34:46', '2021-09-08 23:10:11', '2021-09-08 23:10:11', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('83', '2', '24', '2021-09-08', '2021-09-08 21:38:14', '2021-09-21 21:04:11', '23:25', '103.88.59.67', '103.88.59.67', '0', '25.6130563', '85.1412245', '25.6130801', '85.1411949', '2021-09-08 21:38:14', '2021-09-21 21:04:11', '2021-09-21 21:04:11', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('85', '2', '5', '2021-09-08', '2021-09-08 23:40:35', '2021-09-10 18:23:10', '18:42', '103.88.59.67', '122.167.168.172', '0', '25.6130715', '85.1412224', '12.8827419', '77.6269985', '2021-09-08 23:40:35', '2021-09-10 18:23:10', '2021-09-10 18:23:10', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('86', '2', '5', '2021-09-09', '2021-09-09 01:06:40', '2021-09-09 02:42:44', '1:35', '103.88.59.67', '103.88.59.67', '0', '25.6130789', '85.1412323', '25.6130668', '85.1412263', '2021-09-09 01:06:40', '2021-09-09 02:42:44', '2021-09-09 02:42:44', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('88', '2', '5', '2021-09-09', '2021-09-09 18:37:32', '2021-09-10 18:23:04', '23:45', '122.171.239.147', '122.167.168.172', '0', '12.9342565', '77.6043993', '12.8827419', '77.6269985', '2021-09-09 18:37:32', '2021-09-10 18:23:04', '2021-09-10 18:23:04', '15:54', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('89', '2', '23', '2021-09-09', '2021-09-09 20:31:32', '2021-09-10 05:14:42', '8:42', '103.88.59.67', '157.35.58.90', '0', '25.6130198', '85.1411536', '25.6130095', '85.1411797', '2021-09-09 20:31:32', '2021-09-10 05:14:42', '2021-09-10 05:14:42', '0:9', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('90', '2', '23', '2021-09-10', '2021-09-10 19:52:13', '2021-09-11 05:42:00', '3:49', '103.88.59.67', '103.88.59.67', '0', '25.613036899999997', '85.14121399999999', '25.6130528', '85.14123029999999', '2021-09-10 19:52:13', '2021-09-13 23:42:00', '2021-09-13 23:42:00', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('91', '2', '5', '2021-09-10', '2021-09-10 22:10:12', '2021-09-11 00:10:27', '1:59', '103.88.59.67', '103.88.59.67', '0', '25.613013799999997', '85.14117639999999', '25.6130804', '85.1411973', '2021-09-10 22:10:12', '2021-09-11 00:10:27', '2021-09-11 00:10:27', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('92', '2', '5', '2021-09-11', '2021-09-11 00:19:33', '2021-09-13 10:12:16', '9:52', '103.88.59.67', '122.171.105.225', '0', '25.6129994', '85.1411578', '13.0100959', '77.5945252', '2021-09-11 00:19:33', '2021-09-13 10:12:16', '2021-09-13 10:12:16', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('93', '2', '25', '2021-09-11', '2021-09-11 10:30:47', '2021-09-16 19:19:38', '8:49', '103.88.59.67', '223.187.191.55', '0', '25.6129951', '85.1411689', '25.6190728', '85.1730477', '2021-09-11 02:30:47', '2021-09-16 09:19:38', '2021-09-16 09:19:38', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('94', '2', '5', '2021-09-13', '2021-09-13 10:12:23', '2021-09-16 23:26:55', '13:13', '122.171.105.225', '122.172.21.76', '0', '13.0100959', '77.5945252', '13.0260161', '77.6421465', '2021-09-13 10:12:23', '2021-09-16 23:26:55', '2021-09-16 23:26:55', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('95', '2', '23', '2021-09-13', '2021-09-13 23:45:38', '2021-09-14 05:15:22', '00:00', '103.88.59.67', '1', '0', '25.613077', '85.1411951', '1', '1', '2021-09-13 23:45:38', '2021-09-13 23:45:38', '2021-09-13 23:45:38', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('98', '2', '25', '2021-09-16', '2021-09-16 20:31:43', '2021-09-17 05:30:00', '8:58', '103.88.59.67', '103.88.59.67', '0', '25.613086', '85.1411987', '25.611608', '85.1404734', '2021-09-16 20:31:43', '2021-09-17 05:30:00', '2021-09-17 05:30:00', '0:2', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('99', '2', '23', '2021-09-16', '2021-09-16 20:42:07', '2021-09-17 05:15:57', '8:32', '103.88.59.67', '103.88.59.67', '0', '25.612980099999998', '85.14113480000002', '25.6130866', '85.1412045', '2021-09-16 20:42:07', '2021-09-17 05:15:57', '2021-09-17 05:15:57', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('100', '2', '23', '2021-09-17', '2021-09-17 05:16:20', '2021-09-17 20:21:49', '15:4', '103.88.59.67', '103.88.59.67', '0', '25.6130866', '85.1412045', '25.6130116', '85.1411754', '2021-09-17 05:16:20', '2021-09-17 20:21:49', '2021-09-17 20:21:49', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('101', '2', '25', '2021-09-17', '2021-09-17 20:15:51', '2021-09-20 20:09:55', '23:53', '106.207.55.160', '103.88.59.67', '0', '25.611608', '85.1404734', '25.6130988', '85.1412048', '2021-09-17 20:15:51', '2021-09-20 20:09:55', '2021-09-20 20:09:55', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('102', '2', '25', '2021-09-20', '2021-09-20 23:54:08', '2021-09-21 23:16:25', '23:21', '103.88.59.67', '103.88.59.67', '0', '25.6130539', '85.141205', '25.6130553', '85.1411859', '2021-09-20 23:54:08', '2021-09-21 23:16:25', '2021-09-21 23:16:25', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('103', '2', '25', '2021-09-22', '2021-09-22 02:20:47', '2021-09-22 19:20:35', '16:59', '103.88.59.67', '103.88.59.67', '0', '25.6130734', '85.1412075', '25.6132185', '85.1409768', '2021-09-22 02:20:47', '2021-09-22 19:20:35', '2021-09-22 19:20:35', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('104', '2', '24', '2021-09-22', '2021-09-22 19:17:24', '2021-09-22 19:17:39', '0:0', '103.88.59.67', '103.88.59.67', '0', '25.6131965', '85.1412781', '25.6131965', '85.1412781', '2021-09-22 19:17:24', '2021-09-22 19:17:39', '2021-09-22 19:17:39', '', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('105', '2', '25', '2021-09-22', '2021-09-22 19:20:44', '', '00:00', '103.88.59.67', '1', '0', '25.6132185', '85.1409768', '1', '1', '2021-09-22 19:20:44', '2021-09-22 19:20:44', '2021-09-22 19:20:44', '0:0', 'Present') ; 
INSERT INTO `ci_timesheet` VALUES ('106', '2', '24', '2021-09-22', '2021-09-22 19:46:21', '', '00:00', '103.88.59.67', '1', '0', '25.613199', '85.1412777', '1', '1', '2021-09-22 19:46:21', '2021-09-22 19:46:21', '2021-09-22 19:46:21', '0:28', 'Present') ;
#
# End of data contents of table ci_timesheet
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------


#
# Delete any existing table `ci_timesheet_request`
#

DROP TABLE IF EXISTS `ci_timesheet_request`;


#
# Table structure of table `ci_timesheet_request`
#

CREATE TABLE `ci_timesheet_request` (
  `time_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `request_month` varchar(255) NOT NULL,
  `clock_in` varchar(200) NOT NULL,
  `clock_out` varchar(200) NOT NULL,
  `total_hours` varchar(255) NOT NULL,
  `request_reason` mediumtext NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`time_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_timesheet_request (0 records)
#

#
# End of data contents of table ci_timesheet_request
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------


#
# Delete any existing table `ci_todo_items`
#

DROP TABLE IF EXISTS `ci_todo_items`;


#
# Table structure of table `ci_todo_items`
#

CREATE TABLE `ci_todo_items` (
  `todo_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` mediumtext,
  `is_done` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`todo_item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_todo_items (3 records)
#
 
INSERT INTO `ci_todo_items` VALUES ('1', '2', '5', 'Malabar - Audit 2020-21', '0', '03-08-2021 06:28:31') ; 
INSERT INTO `ci_todo_items` VALUES ('2', '2', '5', 'Useready - Revised Financial & payroll Reconciliation & Inter company Elimanation', '0', '03-08-2021 06:29:15') ; 
INSERT INTO `ci_todo_items` VALUES ('3', '2', '5', 'Nihilent Ananytics - Audit 2020-21', '0', '03-08-2021 06:29:30') ;
#
# End of data contents of table ci_todo_items
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------


#
# Delete any existing table `ci_track_goals`
#

DROP TABLE IF EXISTS `ci_track_goals`;


#
# Table structure of table `ci_track_goals`
#

CREATE TABLE `ci_track_goals` (
  `tracking_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `tracking_type_id` int(200) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `end_date` varchar(200) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `target_achiement` varchar(255) NOT NULL,
  `description` mediumtext,
  `goal_work` text,
  `goal_progress` varchar(200) DEFAULT NULL,
  `goal_status` int(11) NOT NULL DEFAULT '0',
  `goal_rating` int(11) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`tracking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_track_goals (0 records)
#

#
# End of data contents of table ci_track_goals
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------


#
# Delete any existing table `ci_trainers`
#

DROP TABLE IF EXISTS `ci_trainers`;


#
# Table structure of table `ci_trainers`
#

CREATE TABLE `ci_trainers` (
  `trainer_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `expertise` mediumtext NOT NULL,
  `address` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`trainer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_trainers (0 records)
#

#
# End of data contents of table ci_trainers
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training`
# --------------------------------------------------------


#
# Delete any existing table `ci_training`
#

DROP TABLE IF EXISTS `ci_training`;


#
# Table structure of table `ci_training`
#

CREATE TABLE `ci_training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` varchar(200) NOT NULL,
  `training_type_id` int(200) NOT NULL,
  `associated_goals` text,
  `trainer_id` int(200) NOT NULL,
  `start_date` varchar(200) NOT NULL,
  `finish_date` varchar(200) NOT NULL,
  `training_cost` decimal(65,2) DEFAULT NULL,
  `training_status` int(200) DEFAULT NULL,
  `description` mediumtext,
  `performance` varchar(200) DEFAULT NULL,
  `remarks` mediumtext,
  `created_at` varchar(200) NOT NULL,
  PRIMARY KEY (`training_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_training (0 records)
#

#
# End of data contents of table ci_training
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training_notes`
# --------------------------------------------------------


#
# Delete any existing table `ci_training_notes`
#

DROP TABLE IF EXISTS `ci_training_notes`;


#
# Table structure of table `ci_training_notes`
#

CREATE TABLE `ci_training_notes` (
  `training_note_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `training_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `training_note` text,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`training_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_training_notes (0 records)
#

#
# End of data contents of table ci_training_notes
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_transfers`
# --------------------------------------------------------


#
# Delete any existing table `ci_transfers`
#

DROP TABLE IF EXISTS `ci_transfers`;


#
# Table structure of table `ci_transfers`
#

CREATE TABLE `ci_transfers` (
  `transfer_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `transfer_date` varchar(255) NOT NULL,
  `transfer_department` int(111) NOT NULL,
  `transfer_designation` int(11) NOT NULL,
  `reason` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`transfer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_transfers (0 records)
#

#
# End of data contents of table ci_transfers
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_transfers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_travels`
# --------------------------------------------------------


#
# Delete any existing table `ci_travels`
#

DROP TABLE IF EXISTS `ci_travels`;


#
# Table structure of table `ci_travels`
#

CREATE TABLE `ci_travels` (
  `travel_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `employee_id` int(111) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `associated_goals` text,
  `visit_purpose` varchar(255) NOT NULL,
  `visit_place` varchar(255) NOT NULL,
  `travel_mode` int(111) DEFAULT NULL,
  `arrangement_type` int(111) DEFAULT NULL,
  `expected_budget` decimal(65,2) NOT NULL DEFAULT '0.00',
  `actual_budget` decimal(65,2) NOT NULL DEFAULT '0.00',
  `description` mediumtext NOT NULL,
  `status` tinyint(2) NOT NULL,
  `added_by` int(111) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`travel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_travels (0 records)
#

#
# End of data contents of table ci_travels
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_transfers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_travels`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_users_documents`
# --------------------------------------------------------


#
# Delete any existing table `ci_users_documents`
#

DROP TABLE IF EXISTS `ci_users_documents`;


#
# Table structure of table `ci_users_documents`
#

CREATE TABLE `ci_users_documents` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL,
  `document_type` varchar(255) NOT NULL,
  `document_file` varchar(255) NOT NULL,
  `created_at` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_users_documents (0 records)
#

#
# End of data contents of table ci_users_documents
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_transfers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_travels`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_users_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_visitors`
# --------------------------------------------------------


#
# Delete any existing table `ci_visitors`
#

DROP TABLE IF EXISTS `ci_visitors`;


#
# Table structure of table `ci_visitors`
#

CREATE TABLE `ci_visitors` (
  `visitor_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `visit_purpose` varchar(255) DEFAULT NULL,
  `visitor_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `visit_date` varchar(255) DEFAULT NULL,
  `check_in` varchar(255) DEFAULT NULL,
  `check_out` varchar(255) DEFAULT NULL,
  `address` mediumtext,
  `description` mediumtext,
  `created_by` int(11) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`visitor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_visitors (0 records)
#

#
# End of data contents of table ci_visitors
# --------------------------------------------------------

# MySQL database backup
#
# Generated: Thursday 23. September 2021 01:10 IST
# Hostname: localhost
# Database: `suninter_amrhr`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_advance_salary`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_announcements`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_assets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_awards`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_company_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_complaints`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_contract_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_countries`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_database_backup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_departments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_designations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_email_template`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_contacts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_employee_exit`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_company_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_constants`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_settings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_details`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_erp_users_role`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_estimates_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_events`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_accounts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_entity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_membership_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_finance_transactions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_holidays`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_invoices_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_languages`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leads_followup`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_leave_applications`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_meetings`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_membership`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_office_shifts`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_official_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_allowances`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_commissions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_other_payments`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslip_statutory_deductions`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_payslips`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_appraisal_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_performance_indicator_options`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_policies`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_bugs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_projects_timelogs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_candidates`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_interviews`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_rec_jobs`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_recent_activity`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_resignations`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_staff_roles`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_ticket_reply`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_support_tickets`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_system_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_discussion`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_files`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_tasks_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_timesheet_request`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_todo_items`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_track_goals`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_trainers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_training_notes`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_transfers`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_travels`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_users_documents`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_visitors`
# --------------------------------------------------------
# --------------------------------------------------------
# Table: `ci_warnings`
# --------------------------------------------------------


#
# Delete any existing table `ci_warnings`
#

DROP TABLE IF EXISTS `ci_warnings`;


#
# Table structure of table `ci_warnings`
#

CREATE TABLE `ci_warnings` (
  `warning_id` int(111) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `warning_to` int(111) NOT NULL,
  `warning_by` int(111) NOT NULL,
  `warning_date` varchar(255) NOT NULL,
  `warning_type_id` int(111) NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` varchar(255) NOT NULL,
  PRIMARY KEY (`warning_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

#
# Data contents of table ci_warnings (0 records)
#

#
# End of data contents of table ci_warnings
# --------------------------------------------------------

